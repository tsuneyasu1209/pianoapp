class AddCode7Profiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :code_7, :string, null: false, default: "non"

  end
end
