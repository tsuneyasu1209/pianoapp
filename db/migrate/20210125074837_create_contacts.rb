class CreateContacts < ActiveRecord::Migration[6.0]
  def change
    create_table :contacts do |t|
      t.string :name,null: false, default: "名無しさん"
      t.string :email,null: false, default: ""
      t.string :content,null: false, default: ""
      t.timestamps
    end
  end
end
