class AddColumnDate < ActiveRecord::Migration[6.0]
  def change
    add_column :tools, :year, :string, null: false, default: ""
    add_column :tools, :month, :string, null: false, default: ""
    add_column :tools, :day, :string, null: false, default: ""
    add_column :tools, :comment, :string, null: false, default: ""
    add_column :tools, :time, :string, null: false, default: ""
  end
end
