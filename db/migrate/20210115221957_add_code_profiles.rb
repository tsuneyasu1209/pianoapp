class AddCodeProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :code_M, :string, null: false, default: "non"
    add_column :profiles, :code_M7, :string, null: false, default: "non"
    add_column :profiles, :code_m, :string, null: false, default: "non"
    add_column :profiles, :code_m7, :string, null: false, default: "non"
    add_column :profiles, :code_m7_5, :string, null: false, default: "non"
    add_column :profiles, :code_sus4, :string, null: false, default: "non"
    add_column :profiles, :code_dim7, :string, null: false, default: "non"
    add_column :profiles, :code_aug, :string, null: false, default: "non"
    add_column :profiles, :code_6, :string, null: false, default: "non"
    add_column :profiles, :code_add9, :string, null: false, default: "non"
  end
end
