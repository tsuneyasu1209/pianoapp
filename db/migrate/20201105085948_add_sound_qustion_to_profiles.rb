class AddSoundQustionToProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :sound_qestion, :string, null: false, default: "パターン１:c4/d4/e4/f4/g4/b4/a4-パターン2:c5/d5/e5/f5/g5/a5/b5-パターン3:c6/d6/e6/f6/g6/a6/b6"
  end
end
