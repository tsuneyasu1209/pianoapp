class AddColumnText < ActiveRecord::Migration[6.0]
  def change
    add_column :memo, :text, :string, null: false, default: ""
  end
end