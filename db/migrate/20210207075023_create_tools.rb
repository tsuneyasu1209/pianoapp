class CreateTools < ActiveRecord::Migration[6.0]
  def change
    create_table :tools do |t|
      t.string :learning_2021, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2022, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2023, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2024, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2025, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2026, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2027, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2028, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2029, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.string :learning_2030, null: false, default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d"
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
    add_index :tools, [:user_id, :created_at]
  end
end
