class AddColumnToProfile < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :point_score, :string, default: "0"
    add_column :profiles, :point_code, :string, default: "0"
  end
end
