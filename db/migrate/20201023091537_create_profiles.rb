class CreateProfiles < ActiveRecord::Migration[6.0]
  def change
    create_table :profiles do |t|
      t.string :sound, null: false, default: "non"
      t.string :score, null: false, default: "non"
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end
    add_index :profiles, [:user_id, :created_at]
  end
end
