class CreateMemos < ActiveRecord::Migration[6.0]
  def change
    create_table :memo do |t|
      t.string :content, null: false, default: ""
      t.references :user, null: false, foreign_key: true
      t.timestamps
    end
  end
end