class AddCkeyToProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :sound_c, :string, null: false, default: "non"
    add_column :profiles, :sound_g, :string, null: false, default: "non"
    add_column :profiles, :sound_d, :string, null: false, default: "non"
    add_column :profiles, :sound_a, :string, null: false, default: "non"
    add_column :profiles, :sound_e, :string, null: false, default: "non"
    add_column :profiles, :sound_b, :string, null: false, default: "non"
    add_column :profiles, :sound_fp, :string, null: false, default: "non"
    add_column :profiles, :sound_cp, :string, null: false, default: "non"
    add_column :profiles, :sound_f, :string, null: false, default: "non"
    add_column :profiles, :sound_bn, :string, null: false, default: "non"
    add_column :profiles, :sound_en, :string, null: false, default: "non"
    add_column :profiles, :sound_an, :string, null: false, default: "non"
    add_column :profiles, :sound_dn, :string, null: false, default: "non"
    add_column :profiles, :sound_gn, :string, null: false, default: "non"
    add_column :profiles, :sound_cn, :string, null: false, default: "non"

  end
end
