class AddMemoToTools < ActiveRecord::Migration[6.0]
  def change
    add_column :tools, :memo, :string, null: false, default: ""
  end
end
