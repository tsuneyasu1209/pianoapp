class AddColumnTitles < ActiveRecord::Migration[6.0]
  def change
    add_column :theories, :title, :string, null: false, default: ""
  end
end
