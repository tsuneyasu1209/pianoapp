class AddColumnCategory < ActiveRecord::Migration[6.0]
  def change
    add_column :theories, :category, :string, null: false, default: ""
  end
end
