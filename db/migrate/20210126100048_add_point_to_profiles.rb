class AddPointToProfiles < ActiveRecord::Migration[6.0]
  def change
    add_column :profiles, :point, :string, default: "0"
  end
end
