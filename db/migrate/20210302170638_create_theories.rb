class CreateTheories < ActiveRecord::Migration[6.0]
  def change
    create_table :theories do |t|
      t.string :text, null: false, default: ""

      t.timestamps
    end
  end
end
