# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_03_03_060051) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string "name", default: "名無しさん", null: false
    t.string "email", default: "", null: false
    t.string "content", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "memos", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["user_id"], name: "index_memos_on_user_id"
  end

  create_table "plans", force: :cascade do |t|
    t.string "stripe_plan_id", null: false
    t.string "name", null: false
    t.integer "amount", null: false
    t.string "currency", null: false
    t.string "interval", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "profiles", force: :cascade do |t|
    t.string "sound", default: "non", null: false
    t.string "score", default: "non", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "sound_qestion", default: "パターン１:c4/d4/e4/f4/g4/b4/a4-パターン2:c5/d5/e5/f5/g5/a5/b5-パターン3:c6/d6/e6/f6/g6/a6/b6", null: false
    t.string "sound_c", default: "non", null: false
    t.string "sound_g", default: "non", null: false
    t.string "sound_d", default: "non", null: false
    t.string "sound_a", default: "non", null: false
    t.string "sound_e", default: "non", null: false
    t.string "sound_b", default: "non", null: false
    t.string "sound_fp", default: "non", null: false
    t.string "sound_cp", default: "non", null: false
    t.string "sound_f", default: "non", null: false
    t.string "sound_bn", default: "non", null: false
    t.string "sound_en", default: "non", null: false
    t.string "sound_an", default: "non", null: false
    t.string "sound_dn", default: "non", null: false
    t.string "sound_gn", default: "non", null: false
    t.string "sound_cn", default: "non", null: false
    t.string "code_M", default: "non", null: false
    t.string "code_M7", default: "non", null: false
    t.string "code_m", default: "non", null: false
    t.string "code_m7", default: "non", null: false
    t.string "code_m7_5", default: "non", null: false
    t.string "code_sus4", default: "non", null: false
    t.string "code_dim7", default: "non", null: false
    t.string "code_aug", default: "non", null: false
    t.string "code_6", default: "non", null: false
    t.string "code_add9", default: "non", null: false
    t.string "code_7", default: "non", null: false
    t.string "point", default: "0"
    t.string "point_score", default: "0"
    t.string "point_code", default: "0"
    t.index ["user_id", "created_at"], name: "index_profiles_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_profiles_on_user_id"
  end

  create_table "teams", force: :cascade do |t|
    t.string "plan_id"
    t.integer "user_id"
    t.string "stripe_card_id"
    t.string "stripe_customer_id"
    t.string "stripe_subscription_id"
    t.datetime "active_until", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  create_table "theories", force: :cascade do |t|
    t.string "text", default: "", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "title", default: "", null: false
    t.string "category", default: "", null: false
  end

  create_table "tools", force: :cascade do |t|
    t.string "learning_2021", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2022", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2023", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2024", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2025", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2026", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2027", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2028", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2029", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.string "learning_2030", default: "1/:dm2/:dm3/:dm4/:dm5/:dm6/:dm7/:dm8/:dm9/:dm10/:dm11/:dm12/:d", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "memo", default: "", null: false
    t.string "year", default: "", null: false
    t.string "month", default: "", null: false
    t.string "day", default: "", null: false
    t.string "comment", default: "", null: false
    t.string "time", default: "", null: false
    t.index ["user_id", "created_at"], name: "index_tools_on_user_id_and_created_at"
    t.index ["user_id"], name: "index_tools_on_user_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.string "name", default: "", null: false
    t.boolean "admin", default: false
    t.string "provider"
    t.string "uid"
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
    t.index ["unlock_token"], name: "index_users_on_unlock_token", unique: true
  end

  add_foreign_key "memos", "users"
  add_foreign_key "profiles", "users"
  add_foreign_key "tools", "users"
end
