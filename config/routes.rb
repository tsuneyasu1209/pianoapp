Rails.application.routes.draw do
  get 'tools/index'
  devise_for :users, controllers: { 
    sessions: 'users/sessions',
    registrations: "users/registrations",
    omniauth_callbacks: 'users/omniauth_callbacks'
    }

  root 'piano#index'
  get '/questions/sound', to: 'questions#sound'#音感
  get '/questions/score', to: 'questions#score'#楽譜
  get '/questions/code', to: 'questions#code'#コード
  get '/contacts', to: 'contacts#new'#問い合わせ
  get '/tools/metronome', to: 'tools#metronome'#メトロノーム
  get '/tools/watch', to: 'tools#watch'#記録
  get '/tools/code', to: 'tools#code'#記録
  get '/tools/key', to: 'tools#key'#記録
  get '/tools/check', to: 'tools#check'#記録
  get '/tools/checkcode', to: 'tools#checkcode'#記録
  # get '/tools/search', to: 'tools#search', defaults: { format: :json }
  # resources :search, only: :index, defaults: { format: :json }
  resources :theories

  resources :search, only: :index, defaults: { format: :json }
  resources :tttt,          only: [:index, :create, :update, :destroy]
  
  resources :tools,          only: [:create, :update, :destroy]
  resources :contacts,          only: [:create]
  
  namespace :admin do
    resources :contacts, only: [:index]
  end
  namespace :admin do
    resources :users, only: [:index]
  end
  resources :users, only: [:show]
  get '/mypage' => 'users#mypage'
  resources :profiles,          only: [:create, :update, :destroy]
  get 'card/edit'# app/views/card/edit.html.erb画面にアクセス
  post 'card/create'# cardコントローラーのcreateアクション実行

  get 'card/destroy'# app/views/card/destroy.html.erb画面にアクセス
  put 'card/cancel_subscription'# cardコントローラーのcancel_subscriptionアクション実行

  get 'card/restart'# app/views/card/restart.html.erb画面にアクセス
  put 'card/restart_subscription'# cardコントローラーのrestart_subscriptionアクション実行

  get 'card/fin_subscription'# app/views/card/fin_subscription.html.erb画面にアクセス
end
