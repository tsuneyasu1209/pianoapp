class ApplicationController < ActionController::Base
    before_action :configure_permitted_parameters, if: :devise_controller?
    
    private
    def if_not_admin
      if user_signed_in?
        redirect_to root_path unless current_user.admin?
      else
        redirect_to new_user_session_path
      end

    end
    protected  
    def configure_permitted_parameters  
      devise_parameter_sanitizer.permit(:sign_in, keys: [:name])  
      devise_parameter_sanitizer.permit(:sign_up, keys: [:name])  
      devise_parameter_sanitizer.permit(:account_update, keys: [:name]) 
 
    end 
end

