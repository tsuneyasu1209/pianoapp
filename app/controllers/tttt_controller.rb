class TtttController < ApplicationController
    def index
        @tool = Tool.new
        @tools = Tool.where(user_id: current_user.id)
     end
  
      private
      def note_params
        params.require(:tool).permit(:year,:month,:day,:time,:memo).merge(user_id: current_user.id)
      end
end
