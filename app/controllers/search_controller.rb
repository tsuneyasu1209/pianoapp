class SearchController < ApplicationController
    def index
        # ↓検索処理のコード
        if params[:state] == "0"
          @messages = Tool.where(user_id: params[:user_id]).where(year: params[:year]).where(month: params[:month])
        else
          @messages = Tool.where(user_id: params[:user_id]).where(year: params[:year]).where(month: params[:month]).where(day: params[:day])
        end
    respond_to do |format|
      format.html { redirect_to :root }
      # ↓検索結果のデータをレスポンスするコード
      format.json { render json: @messages }
    end
  end
end
