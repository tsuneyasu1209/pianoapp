class UsersController < ApplicationController
  before_action :authenticate_user!, only: [:mypage]
  before_action :set_user, only: [:show]
  
  def mypage
    redirect_to user_path(current_user)
  end
  def show
    # score用のhtml
    st = " major"
    @selet_key_1 = {"G#{st}": 11,"D#{st}": 6,"A#{st}": 1,"E#{st}": 8}
    @selet_key_2 = {"B#{st}": 3,"F##{st}": 10,"C##{st}": 5,"F#{st}": 9,"B♭#{st}": 14}
    @selet_key_3 = {"E♭#{st}": 7,"A♭#{st}": 12,"D♭#{st}": 5,"G♭#{st}":10,"C♭#{st}": 3}
    @selct_code_1 = ["M7","m","m7"]
    @selct_code_2 = ["m7-5","sus4","dim7","aug"]
    @selct_code_3 = ["6", "add9", "7"]
    @error_code = 0
    @profile = Profile.find_by(user_id: current_user.id)
    begin 
    level = @profile.point.chop!
    @level = level.chop!
    rescue => e
      p e.message #=> error
      @level = 0
    end
    begin
      level_s = @profile.point_score.chop!
      @level_s = level_s.chop!
    rescue => e
      @level_s = 0
    end
    begin
      level_c = @profile.point_code.chop!
      @level_c = level_c.chop!
    rescue => e
      @level_c = 0
    end

    @level_amount =@level.to_i + @level_s.to_i + @level_c.to_i
    if  @profile.nil?
      @error_code = 1
      @profile = current_user.profile.build
    end
    @key_list = ['選択して下さい', 'C', 'C#', 'D', 'D#', 'E', 'F', 'F#', 'G', 'G#', 'A', 'A#', 'B']
  end
  private
  
    def set_user
      @user = User.find(params[:id])
    end
end
