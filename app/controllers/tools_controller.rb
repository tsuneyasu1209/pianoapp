class ToolsController < ApplicationController
  before_action :authenticate_user!, only: [:create, :destroy]
  def index
  end

  def metronome
  end

  def watch
    if user_signed_in?
      @tools = Tool.find_by(user_id: current_user.id) 
      if @tools.nil?
        @error_catch = 1
        @tools = current_user.tool.build
      end
    else
  end

  end
  def code
  end
  def key
    
  end

  def check
    
  end
  def checkcode
    
  end
  
  
  def search
      # ↓検索処理のコード
    @messages = Message.where('year LIKE(?)', "%#{params[:title]}%")

    respond_to do |format|
      format.html { redirect_to :root }
      # ↓検索結果のデータをレスポンスするコード
      format.json { render json: @messages }
    end
  end

  def create
    # ajax
    @tool = Tool.new(tool_params)
    if @tool.save
      respond_to do |format|
        format.html { redirect_to tttt_path }
        format.json { render json: { memo: @tool.memo,memo: @tool.year,memo: @tool.month,memo: @tool.day, user_name: @tool.user.name, user_id: @tool.user_id, id: @tool.id } }
      end
    end
  end
  def edit
      tools = Tool.find(current_user.id) if user_signed_in?
  end


    def update
      # ajax
      tool =  Tool.find(params[:id])
        respond_to do |format|
          if tool.update_attributes(tool_params)
            format.html { redirect_to tools_watch_path }
          else
            format.js { render :edit_error }
          end
        end
    end

  def destroy
    @tool = Tool.find(params[:id])
    if @tool.destroy
      respond_to do |format|
        format.html { redirect_to "/tttt" }
        # format.json { render json: { id: params[:id] } }
      end
    end
  end

  def tool_params
    params.require(:tool).permit(:year,:month,:day,:time,:memo).merge(user_id: current_user.id)
  end
end
