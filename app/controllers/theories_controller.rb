class TheoriesController < ApplicationController
  before_action :if_not_admin, except: [:index, :show]
  def index
    @page = "theory"
    @theory = Theory.all.page(params[:page]).per(12)
  end
  def new
    @theory = Theory.new
  end
  def show
    @theory = Theory.find(params[:id])
  end
  def edit
    @theory = Theory.find(params[:id])
  end

  def update
      @theory = Theory.find(params[:id])
      if @theory.update_attributes(theory_params)
        redirect_to theories_path
        # 更新に成功した場合を扱う。
      else
        render 'edit'
      end
    end
  def destroy
    @theory =  Theory.find(params[:id])
    if @theory.destroy
      redirect_to theories_path
    end
  end


  def create
      @theory = Theory.new(theory_params)
      if  @theory.save
        redirect_to theories_path
      else
        @error = 1
        render :new
      end
  end

  def theory_params
      params.require(:theory).permit(:text,:title,:category)
  end
end
