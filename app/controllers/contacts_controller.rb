class ContactsController < ApplicationController
    def index
        @contacts = Contact.all.order(id: "DESC")
        
    end
    def new
        @contact = Contact.new
        @user = User.find(current_user.id) if user_signed_in?
    end
    def create
        def create
             @contact = Contact.new(contact_params)
            if  @contact.save
              flash[:notice] = "お問い合わせを登録しました"
              redirect_to root_url
            else
              @error = 1
              render :new
            end
        end
    end
    def contact_params
        params.require(:contact).permit(:name,:email,:content)
    end
end
