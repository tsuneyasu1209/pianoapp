class PianoController < ApplicationController
  def index
    @theory = Theory.all.limit(8)
    @page = "index"
    @larn_tool = ["音感","楽譜","コード","確率分析"]
    @larn_description = ["ランダムに出される音を聞いてどの音か当てるゲームです。","ランダムに楽譜に描画される音を鍵盤から選んで当てるゲームです。","ランダムに出題されるコードを当てるゲームです。","音感、楽譜、コードで保存した正解率を確認出来ます"]
    @image_path = ["top_ga","top_score","top_code","top_buseki"]
    @url = ['/questions/sound','/questions/score','/questions/code','/mypage']

    @music_tool = ["メトロノーム","コード精製","コード確認","調確認","楽譜、音確認"]
    @tool_url = ['/tools/metronome','/tools/code','/tools/checkcode','/tools/key','/tools/check']
    @tool_description = [
      "指定したBPMで音を刻みます。",
    "ランダムなコードを指定の数だけ精製します。",
    "各コードに使用する鍵盤と和音を確認出来ます。",
    "調によって使う鍵盤を確認出来ます。",
    "鍵盤と楽譜の位置関係を確認出来ます。"
  ]
    @profile = Profile.find_by(user_id: current_user.id) if user_signed_in?

  end
end
