class ProfilesController < ApplicationController
    before_action :authenticate_user!, only: [:create, :destroy]
    def create
        @profile = current_user.profile.build(profile_params)
        if @profile.save
          redirect_to root_url
        else
          render 'questions/sound'
        end
    end
    def edit
        @profile = Profile.find(current_user.id) if user_signed_in?
    end

    def update
        @profile = Profile.find(params[:id])
        @user = User.find(current_user.id)
        if @profile.update_attributes(profile_params)
            redirect_to  @user
          # 更新に成功した場合を扱う。
        else
          render 'edit'
        end
      end
    def destroy
    end
    def profile_params
        params.require(:profile).permit(:sound,:sound_qestion,
          :sound_c,
          :sound_g,
          :sound_d,
          :sound_a,
          :sound_e,
          :sound_b,
          :sound_fp,
          :sound_cp,
          :sound_f,
          :sound_bn,
          :sound_en,
          :sound_an,
          :sound_dn,
          :sound_gn,
          :sound_cn,
          :code_M,
          :code_M7,
          :code_m,
          :code_m7,
          :code_m7_5,
          :code_sus4,
          :code_dim7,
          :code_aug,
          :code_6,
          :code_add9,
          :point,
          :point_score,
          :point_code,
          :learning_2020,
          :learning_2021,
          :learning_2022,
          :learning_2023,
          :learning_2024,
          :learning_2025,
          :learning_2026,
          :learning_2027,
          :learning_2028,
          :learning_2029,
          )
      end
end
