class Admin::ContactsController < ApplicationController
    before_action :if_not_admin
    def index
        keyword = params[:username]
        email = params[:email]
        content = params[:content]
        up_or_down = params[:commit]

        if keyword
             @contacts = Contact.where('name LIKE?', "%#{keyword}%").order(created_at: :desc).page(params[:page]).per(25)
        elsif email
             @contacts = Contact.where('email LIKE?', "%#{email}%").order(created_at: :desc).page(params[:page]).per(25)
        elsif content
            @contacts = Contact.where('content LIKE?', "%#{content}%").order(created_at: :desc).page(params[:page]).per(25)
        elsif up_or_down == "昇順"
            @contacts = Contact.all.page(params[:page]).per(25)
        elsif up_or_down == "降順"
            @contacts = Contact.all.order(created_at: :desc).page(params[:page]).per(25)
        else
            @contacts = Contact.all.order(created_at: :desc).page(params[:page]).per(25)
        end
    end
end