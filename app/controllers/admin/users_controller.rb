class Admin::UsersController < ApplicationController
    before_action :if_not_admin
    def index
        keyword = params[:username]
        email = params[:email]
        up_or_down = params[:commit]
        if keyword
            @users = User.where('name LIKE?', "%#{keyword}%").order(created_at: :desc).page(params[:page]).per(25)
        elsif email
            @users = User.where('email LIKE?', "%#{email}%").order(created_at: :desc).page(params[:page]).per(25)
        elsif up_or_down == "昇順"
            @users = User.all.page(params[:page]).per(25)
        elsif up_or_down == "降順"
            @users = User.all.order(created_at: :desc).page(params[:page]).per(25)
        else
            @users = User.all.order(created_at: :desc).page(params[:page]).per(25)
        end
        @count = User.count
    end
    
end
