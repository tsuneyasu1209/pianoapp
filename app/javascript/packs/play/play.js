import {
    con, cs
} from "../common/common";



window.AudioContext = window.AudioContext || window.webkitAudioContext;
const ctx = new AudioContext();
let sampleSource;
// 再生中のときはtrue
let isPlaying = false;

export async function setupSample(key_list) {
    let return_list = []
    for (let i = 0; i < key_list.length; i++) {
        const target = document.querySelector(`audio[data-key="${key_list[i]}"]`).getAttribute('src')
        const response = await fetch(target);
        const arrayBuffer = await response.arrayBuffer();
        // Web Audio APIで使える形式に変換
        return_list.push(await ctx.decodeAudioData(arrayBuffer))
    }
    return return_list;
}


// AudioBufferをctxに接続し再生する関数
function playSample(ctx, audioBuffer) {
    sampleSource = ctx.createBufferSource();
    // 変換されたバッファーを音源として設定
    sampleSource.buffer = audioBuffer;
    // 出力につなげる
    sampleSource.connect(ctx.destination);

    sampleSource.start();
    isPlaying = true;
}
// export async function keyPlay(key_list) {

//     let play_list = await setupSample(key_list)
//     for (let i = 0; i < play_list.length; i++) {
//         playSample(ctx, play_list[i]);
//     }
// }
export function keyPlay(key) {
    wa.playSilent();
    for (let i = 0; i < key.length; i++) {
        wa.play(key[i]);
    }
}


export let all_key = ["a0", "ap0", "b0", "c8", "c1", "d1", "e1", "f1", "g1", "a1", "b1", "cp1", "dp1", "fp1", "gp1", "ap1", "c2", "d2", "e2", "f2", "g2", "a2", "b2", "cp2", "dp2", "fp2", "gp2", "ap2", "c3", "d3", "e3", "f3", "g3", "a3", "b3", "cp3", "dp3", "fp3", "gp3", "ap3", "c4", "d4", "e4", "f4", "g4", "a4", "b4", "cp4", "dp4", "fp4", "gp4", "ap4", "c5", "d5", "e5", "f5", "g5", "a5", "b5", "cp5", "dp5", "fp5", "gp5", "ap5", "c6", "d6", "e6", "f6", "g6", "a6", "b6", "cp6", "dp6", "fp6", "gp6", "ap6", "c7", "d7", "e7", "f7", "g7", "a7", "b7", "cp7", "dp7", "fp7", "gp7", "ap7"]

export async function loadAllKey(key_list = all_key) {

    let play_list = await setupSample(key_list)
    return play_list
}
class TopCodeOpening {
    constructor() {
        // ミニ鍵盤に色を塗る
        this.plus_num_list = [
            [4, 3], // M
            [4, 3, 4], //M7
            [3, 4], //m
            [3, 4, 3], //m7
            [3, 3, 4], //m7-5
            [5, 2], //sus4
            [3, 3, 3], //dim7
            [4, 4], //aug
            [4, 3, 2], //6
            [4, 3, 7], //add9
            [4, 3, 3], //7
        ];
        this.audio_key = [
            "c4",
            "cp4",
            "d4",
            "dp4",
            "e4",
            "f4",
            "fp4",
            "g4",
            "gp4",
            "a4",
            "ap4",
            "b4",
            "c5",
            "cp5",
            "d5",
            "dp5",
            "e5",
            "f5",
            "fp5",
            "g5",
            "gp5",
            "a5",
            "ap5",
            "b5",
            "c6",
            "cp6",
            "d6",
        ];
    }

    calCodeNum(key_num_list, base_num) {
        // codeに使う鍵盤番号を計算
        let result_num_list = [];
        let last_result_list = [];
        for (let i = 0; i < key_num_list.length; i++) {
            let cal_num = base_num;
            result_num_list = [base_num];
            for (let k = 0; k < key_num_list[i].length; k++) {
                cal_num += key_num_list[i][k];
                result_num_list.push(cal_num);
            }
            last_result_list.push(result_num_list);
        }
        return last_result_list;
    }

    playCode(list) {
        let play_list = [];
        for (let i = 0; i < list.length; i++) {
            let num = list[i];
            let a = play_code["audio_key"][num - 1];
            play_list.push(a);
        }
        keyPlay(play_list);
    }
}



export let play_code = new TopCodeOpening()