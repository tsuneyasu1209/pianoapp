import {
  con,
  ev,
  cs,
  unDrowYellow
} from "../common/common";
import {
  com
} from "../common/func";

import {
  questionsSoundPlay,
  make_questions_selected_times,
} from "../common/sound";

import {
  keyEv
} from "../common/key";
import {
  sco
} from "../common/socre";
import {
  cal,
  show
} from "../common/rate";
import {
  PointBar
} from "../common/point_bar";
import {
  keyPlay,
  loadAllKey,
  all_key
} from "../play/play";
let symbol;
let chose_registed_sound_question = 0;
let point = 0;
let login = com.checUserLogin();

let all = loadAllKey()


cs.attachOrDetach(".vol_right");
cs.cahgeCss('.vol_block', "right: 30px;")


class Select {
  // 使う鍵盤の範囲を選択
  select_range() {
    let start_num = 0;
    let end_num = 0;
    let start = document.getElementById("start_target");
    let end = document.getElementById("end_target");
    let key_select_btns = document.querySelectorAll(".key_select_btn");
    let one_more_btns = document.querySelectorAll(".one_more");
    let error = document.querySelector(".select_error");
    // 問題数の取得
    let questions_num_targets = document.getElementsByName("question_num");

    cs.cahgeCss(".select_block", "display: none");
    cs.cahgeCss("sore_content", "display: block");

    // ゲームをスタートさせるフラグ
    let game_start_flag = 0;

    const piano_wrap = document.querySelector(".piano_wrap");
    piano_wrap.style.cssText = "display: block";
    error.style.cssText = "display: none";
    start_num = start.value.substr(1, 1);
    end_num = end.value.substr(1, 1);
    if (start_num <= end_num) {
      for (let i = 0; i < key_select_btns.length; i++) {
        key_select_btns[i].style.cssText = "display: block";
      }



      // ゲームをスタートさせるフラグ有効化
      game_start_flag = 1;

      // 問題数の取得
      questions_num_targets.forEach((questions_num_target) => {
        if (questions_num_target.checked) {
          questions_num = questions_num_target.value;
        }
      });

      for (let i = 0; i < one_more_btns.length; i++) {
        one_more_btns[i].style.cssText = "display: block";
      }
    } else {
      error.style.cssText = "display: block";
    }
    return [game_start_flag, start_num, end_num];
  }

  // 問題を作る
  createArray(start_num, end_num) {
    num_array = [];
    for (let i = start_num; i <= end_num; i++) {
      num_array.push(i);
    }
  }

  changeSelect() {

    let w_or_b_vlue = ""
    let w_or_b = document.getElementsByName("selected_key")
    for (let i = 0; i < w_or_b.length; i++) {
      if (w_or_b[i].checked == true) {
        w_or_b_vlue = w_or_b[i].value
      }
    }
    let start = document.getElementById("start_target");
    let end = document.getElementById("end_target");
    let error = document.querySelector(".select_error");
    let start_num = parseInt(start.value.substr(1, 1), 10);
    let end_num = parseInt(end.value.substr(1, 1), 10);

    error.style.cssText = "display: none";

    function dorwColor(
      opacity = "opacity: 1",
      color = "#FAF5E1",
      de_start = 0,
      de_end = 9
    ) {
      for (let i = de_start; i < de_end; i++) {
        let keys = document.querySelectorAll(`.mini_key_${i}`);
        for (let k = 0; k < keys.length; k++) {
          let id = keys[k].id.indexOf("p")
          if (w_or_b_vlue == "white") {
            if (id == -1) {
              keys[k].style.cssText = opacity;
              keys[k].style.backgroundColor = color;
            }
          } else {
            keys[k].style.cssText = opacity;
            keys[k].style.backgroundColor = color;
          }
        }
      }
    }
    if (start_num <= end_num) {
      unDrowYellow(".skey");
      dorwColor("opacity: 1", "#FC001C", start_num, end_num + 1);
    } else {
      error.style.cssText = "display: block";
    }
  }
  changeRegstrationQuestion(e) {
    // 保存した問題をリストに入れる
    registed_question = use_list[e.target.value][1];
  }
  selectRegistedQuestion(e) {
    // 保存した問題を使用するかの選択
    chose_registed_sound_question = e.target.value;
    let loop_list = [
      "registered_sound_question_select_box_under",
      "registered_sound_question_select_box",
    ];
    cs.switchDisplay(loop_list, chose_registed_sound_question);
  }
}
let sel = new Select();
let point_bar = new PointBar(login, "point_form_edit");
class Judge {
  // 正解のジャッジ
  judgeAnser(question_array, anser_array) {
    let judge;
    let count_corrct = 0;
    for (let i = 0; i < question_array.length; i++) {
      if (question_array[i] == anser_array[i]) {
        judge = "ok";
        count_corrct++;
      } else {
        judge = "no";
      }
      if (times_of_anser == 1) {
        cal.calculateCodeReusult(judge, result_list, question_array[i]);
        point = point_bar.calculatePoint(judge, point);
        point_bar.levleBar(judge, point_bar["point_list"]); //レベルバーの更新
      }
    }

    if (count_corrct == question_array.length) {
      times_of_anser = 1;
      judge = "ok";
      point_bar.insertProgressBar(questions_count); //進行度のバーを更新
      questions_count++;
      cs.insertQuestionsNum("insert_question_num", questions_count);
    } else {
      times_of_anser++;
      judge = "no";
    }
    return judge;
  }

  // ○☓消す処理
  // 答え合わせした後の処理
  next_question(a) {
    // 正解の時の処理
    if (a == "ok") {
      question_array = make_questions_selected_times(registed_question);
      setTimeout(function () {
        questionsSoundPlay(question_array);
      }, 100);
    }
    // 不正解の時の処理
    else if (a == "no") {
      setTimeout(function () {
        questionsSoundPlay(question_array);
      }, 100);
    }
  }
}
let jud = new Judge();

class Anser {
  constructor(target) {
    // 高音域の８VA
    let va = 0;
    target = keyEv.singeKeyAnser(
      octave,
      target,
      question_array[score_count - 1]
    );
    anser_array.push(target);
    symbol = sco.Judgment_of_sound(target);
    va = sco.Judgment_of_8va(target);
    let key = [target];
    keyPlay(key);
    // 携帯用
    sco.drawScore(target, va, score_count, symbol);
    score_count++;
    // 音の数の取得
    const anser_nums = document.getElementsByName("selected_sound_num");
    anser_nums.forEach((anser_num) => {
      if (anser_num.checked) {
        checked_anser_num = anser_num.value;
      }
    });
  }
}
class AfterAnser {
  constructor() {
    // 指定した数の問題数が終わった後の処理
    keyEv.ratePageKeyBtns();
    try {
      show.showRate();
    } catch (error) {}
    // 結果出た後の聞けるようにする処理
    ev.eventTargetAll(".sound_result", "click", show.selecteRate);
    ev.eventTargetAll(".sound_result", "mouseover", show.dorwKeyColor);
    ev.eventTargetAll(".sound_result", "mouseout", show.deleteKeyColor);
    point_bar.insertPointForm(point);
    questions_count = 1;
  }
}

function select_key(e) {
  let target = e.target.id;
  new Anser(target);
  // 指定の数を数え終わったら
  if (checked_anser_num == anser_array.length) {
    // 正解のジャッジ
    let a = jud.judgeAnser(question_array, anser_array);

    if (questions_num >= questions_count) {
      // 答え合わせした後の処理 選択した問題数だけ繰り返す
      com.showJudgement(a);
      jud.next_question(a);
    } else {
      new AfterAnser();
    }
    setTimeout(sco.deleteScore, 800, anser_array, symbol);
    anser_array = [];
    score_count = 1;
  }
}

function gameStart() {
  wa.playSilent();
  let select_range_content = sel.select_range();
  point_bar["progress_with"] = point_bar.getProgressWidth(questions_num);

  if (select_range_content[0] == 1) {
    sel.createArray(select_range_content[1], select_range_content[2]);
    if (chose_registed_sound_question == 0) {
      registed_question = "";
    }
    question_array = make_questions_selected_times(registed_question);
    questionsSoundPlay(question_array);
  }
  cs.attachOrDetach(".mini_piano")
}
// オクターブで使う鍵盤の数を決める
ev.eventTargetAll(".select_octave", "click", keyEv.swithOctave);
ev.eventTargetId("sound_start_btn", "click", gameStart);
ev.eventTargetId("start_target", "click", sel.changeSelect);
ev.eventTargetId("end_target", "click", sel.changeSelect);
ev.eventTargetAll(".key", "click", select_key);
// 登録した問題
ev.eventTargetAll(
  ".select_registed_sound_question",
  "click",
  sel.selectRegistedQuestion
);
let registed_question = "";
let use_list = "";
try {
  use_list = com.sliceQuestions("registered_sound_question_input");
  com.insertSelectBoxContent(use_list, "registered_sound_question_select_box");
} catch (error) {}
try {
  ev.eventTargetId(
    "registered_sound_question_select_box",
    "change",
    sel.changeRegstrationQuestion
  );
} catch (error) {}