import {
    con,
    ev,
    cs
} from "../../common/common";
let text_ob = {}
text_ob.categoly = ""
text_ob.h1 = ["音の形、音符や休符について", "このページではよくオタマジャクシと表現される音符についての解説をしています。"]
// tip_titlesとtip_contentsの数が同じで無いとエラー
text_ob.tip_titles = ["音符"];
text_ob.tip_contents = [
    ["音符","休符","対比表","付点音符"],
];

text_ob.is_text = [
    [
        `@@<div class="theory_image_box"><img src="/images/theory/onpu/sibuo.png" alt=""></div>`,
        `形からよくオタマジャクシなんて表現されることもあり、見た事有る方も多いのでは無いでしょうか？<br>形によって@ss音を鳴らす時間@seが変わります。`
    ],
    [
        `@@<div class="theory_image_box"><img src="/images/theory/onpu/sibuk.png" alt=""></div>`,
        `こちらはもしかしたら見たこと無い方もいるかもしれません、今度は逆に@ss音を鳴らさない時間@seを表した物になります。`
    ],
    [
        `@@<div class="theory_image_box"><img src="/images/theory/onpu/notetable.png" alt=""></div>`
    ],
    [
        `<div class="theory_image_box"><img src="/images/theory/onpu/futen.png" alt=""></div>`,
        `音符に「.」がついている場合があり、これを付点音符と言います。<br>
        付点が付いている@ss音符の半分の長さを足した物が付点音符@seになり、2分音符に付点が付くと、上の様な結果になります。`
    ]
]

text_ob.summary = [
    "音程は２つの音の高さのへだたりを表した物",
    "半音とは一つ隣の音",
    "全音とは半音２つ",
    "完全系音程は1度・4度・5度・8度",
    "＃や♭がついた場合、減5度、増5度になる。",
    "長短系は長音程と短音程に別けられる",
    "長短系音程は2度・3度・6度・7度",
    "短音程は長音程を半音下げたもの"
]

function insertTips(tag1 = ".w94", tag2 = "insert_tip", tag3 = "insert_content") {
    // 目次を作る
    let html = "";
    for (let i = 0; i < text_ob["h1"].length; i++) {
        // タイトル挿入
        if (i == 0) {
            html += `<h1 class="ch1 del_theoy">${text_ob["h1"][i]}</h1>`
        } else {
            html += `<p class="ctx  del_theoy">${text_ob["h1"][i]}</p>`
        }

    }
    document.querySelector(tag1).insertAdjacentHTML("afterbegin", html);
    html = "";
    let data_count = 0;
    for (let i = 0; i < text_ob["tip_titles"].length; i++) {
        // 目次挿入
        html += `<li class="li_title tip_link del_theoy" data-tips="${data_count}">${text_ob["tip_titles"][i]}</li>`;
        data_count++;
        for (let k = 0; k < text_ob["tip_contents"][i].length; k++) {
            html += `<li class="li_content tip_link del_theoy" data-tips="${data_count}">${text_ob["tip_contents"][i][k]}</li>`;
            data_count++;
        }
    }
    document.getElementById(tag2).insertAdjacentHTML("afterbegin", html);
    // ブロック挿入
    html = ""
    for (let i = 0; i < text_ob["tip_titles"].length; i++) {
        html += `<h2 class="ch cl del_theoy">${text_ob["tip_titles"][i]}</h2>`;
        for (let k = 0; k < text_ob["tip_contents"][i].length; k++) {
            html += `<h3 class="ct cl del_theoy">${text_ob["tip_contents"][i][k]}</h3>`;
        }
    }
    document.getElementById(tag3).insertAdjacentHTML("afterbegin", html);
}

function insertText(tag1 = ".ct") {
    // テキスト挿入
    let html = ""
    let targets = document.querySelectorAll(tag1)
    for (let k = 0; k < targets.length; k++) {
        html = ""
        for (let i = 0; i < text_ob["is_text"][k].length; i++) {
            if (text_ob["is_text"][k][i] != "") {

                let a = text_ob["is_text"][k][i].slice(0, 2)
                const replaced = text_ob["is_text"][k][i].replace(/@ss/g, '<strong>')
                const result = replaced.replace(/@se/g, '</strong>')
                if (a == "@@") {
                    html += result.slice(2)
                } else {

                    html += `<p class="ctx del_theoy">${result}</p>`
                }
            }

        }
        targets[k].insertAdjacentHTML("afterend", html)
    }
}

function insertSummary(tag1 = "insert_summary") {
    let html = ""

    for (let i = 0; i < text_ob["summary"].length; i++) {
        html += `<li class="li_title del_theoy">${text_ob["summary"][i]}</li>`;
    }

    document.getElementById(tag1).insertAdjacentHTML("afterbegin", html);
}

function getHtml() {
    let html = document.querySelector(".w94").innerHTML
    document.getElementById("theory_title").value = document.querySelector(".ch1").textContent
    html = html.replace(/\r?\n/g, "")
    html = html.replace(/                    /g, "")
    html = html.replace(/    /g, "")

    document.getElementById("theory_text").value = html
    document.getElementById("theory_category").value = text_ob["categoly"]


}

function addContent(ele, html_class, row) {
    const target = document.querySelector(ele)
    const html = `<textarea class="${html_class}" cols="50" rows="${row}"></textarea><button class="del_content">del</button>`
    target.insertAdjacentHTML("beforeend", html)
}


function delContent(e) {

    const del_btn = e.target
    const del = del_btn.previousElementSibling;
    del_btn.remove()
    del.remove()
}

function removePTag(target) {
    let del_t_t = document.querySelectorAll(target)
    for (let i = 0; i < del_t_t.length; i++) {
        del_t_t[i].remove()
    }
}

function insertPTag(target, html_class, text) {
    let insert = document.querySelectorAll(target)

    for (let i = 0; i < insert.length; i++) {
        let html = `<p class="${html_class}">${text[i]}</p>`
        insert[i].insertAdjacentHTML("beforebegin", html)
    }

}

function changeContent() {
    const is_text = document.querySelectorAll(".is_text").length
    let text = []
    let num = 0
    let tips = document.querySelectorAll(".tip_contents")
    for (let i = 0; i < tips.length; i++) {
        let a = tips[i].value.split("^^")
        num += a.length
        for (let k = 0; k < a.length; k++) {
            text.push(a[k])
        }
    }
    // テキスト部分

    if (num != is_text) {

        let times = num - is_text
        for (let i = 0; i < times; i++) {
            addContent(".insert_is_text", "is_text", "4")

        }
        ev.eventTargetAll(".del_content", "click", delContent)

        removePTag(".del_t_t")
        insertPTag(".is_text", "del_t_t", text)

    }

}

function chageTitle() {
    text_ob["tip_titles"] = document.getElementById("tip_titles").value.split("^^")
    let tips = document.querySelectorAll(".tip_contents").length

    if (text_ob["tip_titles"].length != tips) {

        let times = text_ob["tip_titles"].length - tips
        for (let i = 0; i < times; i++) {
            // 目次の項目フォーム追加
            addContent(".insert_tip_contens", "tip_contents", "4")
        }
        removePTag(".del_t_c")
        insertPTag(".tip_contents", "del_t_c", text_ob["tip_titles"])
        ev.eventTargetAll(".del_content", "click", delContent)
        ev.eventTargetAll(".tip_contents", "change", changeContent)

    }

}
class GetText {
    constructor() {
        ev.eventTargetAll(".theory_input_btn", "click", this.addContent)
        ev.eventTargetId("tip_titles", "change", chageTitle)
        ev.eventTargetId("preview", "click", this.getText)
        ev.eventTargetId("theory_edit_btn", "click", this.openEdit)
    }

    addContent(e) {
        let target
        let html = ""
        if (e.target.id == "add_content") {
            target = document.querySelector(".insert_tip_contens")
            html += `<textarea class="tip_contents" cols="50" rows="4"></textarea></textarea><button class="del_content">del</button>`
        } else if (e.target.id == "add_text") {
            target = document.querySelector(".insert_is_text")
            html += `<textarea class="is_text" cols="50" rows="4"></textarea></textarea><button class="del_content">del</button>`
        }

        target.insertAdjacentHTML("beforeend", html)
    }


    getText() {
        text_ob["h1"] = document.getElementById("h1_input").value.split("^^")
        text_ob["tip_titles"] = document.getElementById("tip_titles").value.split("^^")
        const tip_contents = document.querySelectorAll(".tip_contents")
        let contents = []
        for (let i = 0; i < tip_contents.length; i++) {
            contents.push(tip_contents[i].value.split("^^"))
        }
        text_ob["tip_contents"] = contents
        let texts = []
        const is_text = document.querySelectorAll(".is_text")
        for (let i = 0; i < is_text.length; i++) {
            texts.push(is_text[i].value.split("^^"))
        }
        text_ob["is_text"] = texts
        text_ob["summary"] = document.getElementById("summary").value.split("^^")
        const del = document.querySelectorAll(".del_theoy")
        for (let i = 0; i < del.length; i++) {
            del[i].remove()

        }
        insertTips();
        insertText()
        insertSummary()
        getHtml()
        con("-------------")
        con(text_ob["categoly"])
        con(text_ob["h1"])
        con(text_ob["tip_titles"])
        con(text_ob["tip_contents"])
        con(text_ob["is_text"])
        con(text_ob["summary"])
   

        document.getElementById("insert_save_text").value =text_ob["tip_contents"]
        cs.attachOrDetach(".js_theory_block")
        cs.attachOrDetach("theory_edit_btn",1)
        
    }
    openEdit(){
        cs.attachOrDetach(".js_theory_block",1)
        cs.attachOrDetach("theory_edit_btn")
    }
}
new GetText()


// insertTips();
// insertText()
// insertSummary()
// getHtml()