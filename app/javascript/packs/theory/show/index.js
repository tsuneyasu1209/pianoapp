import {
    ev
} from "../../common/common"

function insetHtml() {
    let time = `<div class="flex"><img class="up_icon" src="/up_icon.png">`
    time += document.getElementById("update_time").textContent
    time += '</div>'
    let html = document.getElementById("target_html").textContent
    document.getElementById("insert_html").insertAdjacentHTML("afterbegin", html);
    document.getElementById("insert_html").insertAdjacentHTML("afterbegin", time);
}
insetHtml()

function selctTips(e) {
    // 目次で移動
    let tips = document.querySelectorAll(".cl");
    let index = parseInt(e.target.dataset.tips);
    let rect = tips[index].getBoundingClientRect();
    var position = rect.top;
    scrollTo(0, position);
}
ev.eventTargetAll(".tip_link", "click", selctTips);