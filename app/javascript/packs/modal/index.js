import {
    cs
} from "../common/common"
let load_count = 0
const insert_persent = document.getElementById("insert_persent")


;
(function (window) {

    var wa = {

        context: null,
        _buffers: {},


        _initialize: function () {
            this.context = new(window.AudioContext || window.webkitAudioContext)();
        },

        playSilent: function () {
            var context = this.context;
            var buf = context.createBuffer(1, 1, 22050);
            var src = context.createBufferSource();
            src.buffer = buf;
            src.connect(context.destination);
            src.start(0);
        },

        play: function (buffer) {
            // ファイル名で指定
            if (typeof buffer === "string") {
                buffer = this._buffers[buffer];
                if (!buffer) {
                    console.error('ファイルが用意できてません!');
                    return;
                }
            }

            var context = this.context;
            var source = context.createBufferSource();
            source.buffer = buffer;
            source.connect(context.destination);
            source.start(0);
        },

        loadFile: function (src, cb) {
            var self = this;
            var context = this.context;
            var xml = new XMLHttpRequest();
            xml.open('GET', src);
            xml.onreadystatechange = function () {
                if (xml.readyState === 4) {
                    if ([200, 201, 0].indexOf(xml.status) !== -1) {

                        var data = xml.response;

                        // webaudio 用に変換
                        context.decodeAudioData(data, function (buffer) {
                            // buffer登録
                            var s = src.split('/');
                            var b = s[s.length - 1];
                            let splited = b.split("-")
                            let key = splited[0]
                            self._buffers[key] = buffer;
                            // コールバック
                            cb(buffer);
                            load_count++
                            let num = Math.floor(load_count / 88 * 100)
                            let text = num + "%"
                            insert_persent.textContent = text
                            if (num == 100) {
                                insert_persent.classList.add("bounce")
                                setTimeout(() => {
                                    cs.attachOrDetach(".l_modal")
                                }, 1000);
                            }
                        });

                    } else if (xml.status === 404) {
                        // not found
                        console.error("not found");
                    } else {
                        // サーバーエラー
                        console.error("server error");
                    }
                }
            };

            xml.responseType = 'arraybuffer';

            xml.send(null);
        },

    };

    wa._initialize(); // audioContextを新規作成

    window.wa = wa;

}(window));

function modal() {
    let target = document.querySelector(".l_modal")
    if (window.scrollY >= 0) {
        target.style.paddingTop = window.scrollY + "px";
    } else {
        target.style.paddingTop = 0 + "px";
    }

}
window.onload = function () {
    // ページ読み込みと同時にロード
    let all = ["a0", "ap0", "b0", "c8", "c1", "d1", "e1", "f1", "g1", "a1", "b1", "cp1", "dp1", "fp1", "gp1", "ap1", "c2", "d2", "e2", "f2", "g2", "a2", "b2", "cp2", "dp2", "fp2", "gp2", "ap2", "c3", "d3", "e3", "f3", "g3", "a3", "b3", "cp3", "dp3", "fp3", "gp3", "ap3", "c4", "d4", "e4", "f4", "g4", "a4", "b4", "cp4", "dp4", "fp4", "gp4", "ap4", "c5", "d5", "e5", "f5", "g5", "a5", "b5", "cp5", "dp5", "fp5", "gp5", "ap5", "c6", "d6", "e6", "f6", "g6", "a6", "b6", "cp6", "dp6", "fp6", "gp6", "ap6", "c7", "d7", "e7", "f7", "g7", "a7", "b7", "cp7", "dp7", "fp7", "gp7", "ap7"]
    for (let i = 0; i < all.length; i++) {
        let target = document.querySelector(`audio[data-key="${all[i]}"]`).getAttribute('src')
        wa.loadFile(`${target}`, function (buffer) {});
    }

}
window.addEventListener("scroll", modal);