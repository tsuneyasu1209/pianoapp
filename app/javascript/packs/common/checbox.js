import { cs, con } from "./common";
export class CheckBox {
  checkError(target) {
    // チェックボックスんの数を数えてチェックされてなければエラーを出す
    const targets = document.querySelectorAll(target);
    let count_num = 0;
    for (let i = 0; i < targets.length; i++) {
      if (targets[i].checked == true) {
        count_num++;
      }
    }
    if (count_num == 0) {
      cs.attachOrDetach(`${target}_error`, 1);
    } else {
      cs.attachOrDetach(`${target}_error`);
    }
    return count_num;
  }
  allCheckOrUncheck(check_list, target) {
    // listに入ってるチェックボックスを全て選択、又ははずす
    const all_check = document.querySelector(".all_check");
    const all_uncheck = document.querySelector(".all_uncheck");
    function check(params, true_or_false) {
      for (let i = 0; i < params.length; i++) {
        const all_check_box = document.querySelectorAll(`${params[i]}`);
        for (let i = 0; i < all_check_box.length; i++) {
          all_check_box[i].checked = true_or_false;
        }
      }
    }
    if (target.classList.contains("all_check") && target.checked) {
      check(check_list, true);
      all_uncheck.checked = false;
    } else if (target.classList.contains("all_uncheck") && target.checked) {
      check(check_list, false);
      all_check.checked = false;
    }
  }

  changeChecked(params, target) {
    // 一つだけしかチェック出来ないようにする

    const all_check_box = document.querySelectorAll(`${params}`);
    for (let i = 0; i < all_check_box.length; i++) {
      if (all_check_box[i] != target) {
        all_check_box[i].checked = false;
      }
    }
  }

  checkUnckeck(target) {
    // 全て選ぶと全て外すの挙動
    let targets = document.querySelectorAll(target);
    let check_count = 0;
    for (let i = 0; i < targets.length; i++) {
      if (targets[i].checked == true) {
        check_count++;
      }
    }
    const all_check_btn = document.querySelector(".all_check");
    const all_uncheck_btn = document.querySelector(".all_uncheck");
    if (check_count == targets.length) {
      // 全て押された時
      all_check_btn.checked = true;
      all_uncheck_btn.checked = false;
    } else if (check_count == 0) {
      // 何も選択されてない時
      all_uncheck_btn.checked = true;
    } else {
      // 一つ以上選択されている時
      all_check_btn.checked = false;
      all_uncheck_btn.checked = false;
    }
  }
  allClreCheckBox(ele) {
    // 全チェック外す
    const target = document.querySelectorAll(ele);
    for (let i = 0; i < target.length; i++) {
      target[i].checked = false;
    }
  }
}
export let ck = new CheckBox();
