class Score {
  Judgment_of_sound(key) {
    let symbol;
    let a = key.slice(-1);
    let random_list;
    if (a >= 5) {
      symbol = "toon_";
    } else if (a <= 2) {
      symbol = "heon_";
    } else if (a <= 3) {
      if (key.match(/c/) || key.match(/d/)) {
        symbol = "heon_";
      } else {
        random_list = ["toon_", "heon_"];
        symbol = random_list[Math.floor(Math.random() * 2)];
      }
    } else {
      if (key.match(/b/) || key.match(/a/)) {
        symbol = "toon_";
      } else {
        random_list = ["toon_", "heon_"];
        symbol = random_list[Math.floor(Math.random() * 2)];
      }
    }
    return symbol;
  }
  // 8vaの判断
  Judgment_of_8va(key) {
    let va = 0;
    if (key.match(/7/) || key.match(/8/) || key.match(/b6/)) {
      va = 1;
    }
    return va;
  }
  // 楽譜に描画
  drawScore(score, va, score_count, symbol = "") {
    let key = `.${symbol}score_${score}_${score_count}`;
    let scores = document.querySelectorAll(`${key}`);
    let symbol_toons = document.querySelectorAll(
      `.symbol_${symbol}${score_count}`
    );
    let symbol_heons = document.querySelectorAll(
      `.symbol_${symbol}${score_count}`
    );
    if (va == 1) {
      let va8 = document.querySelectorAll(`.va_${score_count}`);
      va8.forEach((s) => {
        s.style.cssText = "display: block";
      });
    }

    if (key.match(/p/)) {
      let sharps = document.querySelectorAll(
        `.${symbol}sharp_score_${score}_${score_count}`
      );
      sharps.forEach((s) => {
        s.style.cssText = "display: block";
      });
    }

    scores.forEach((s) => {
      s.style.cssText = "display: block";
    });
    symbol_toons.forEach((s) => {
      s.style.cssText = "display: block";
    });
    symbol_heons.forEach((s) => {
      s.style.cssText = "display: block";
    });
  }
  // 楽譜の消去
  deleteScore() {
    let al = document.querySelectorAll(".del");
    let flat_sharp = document.querySelectorAll(".flat_sharp");
    al.forEach((s) => {
      s.style.cssText = "display: none";
    });
    for (let i = 0; i < flat_sharp.length; i++) {
      flat_sharp[i].classList.add("vanish");
    }
    try {
      va = 0;
    } catch (error) {}
  }
}
export let sco = new Score();
