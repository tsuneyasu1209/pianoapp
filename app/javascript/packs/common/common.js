import { com } from "../common/func";
export function con(params) {
  console.log(params);
}
export function getTarget(name) {
  let target;
  let name_of_head = name.slice(0, 1);
  if (name_of_head === ".") {
    target = document.querySelector(`${name}`);
  } else {
    target = document.getElementById(`${name}`);
  }
  return target;
}

// cssを扱うクラス
class Css {
  cahgeCss(name, chage_css) {
    const target = getTarget(name);
    target.style.cssText = `${chage_css}`;
  }
  insertQuestionsNum(target, questions_count) {
    const element = document.getElementById(`${target}`);
    element.textContent = `第${questions_count}問`;
  }

  attachOrDetach(name, order = 0) {
    // display 0:none 1:block 2: toggle
    const target = getTarget(name);
    switch (order) {
      case 0:
        target.classList.add("vanish");
        break;
      case 1:
        target.classList.remove("vanish");
        break;
      case 2:
        target.classList.toggle("vanish");
        break;
    }
  }
  multipleAtOrDt(target_name_list, order = 0) {
    // 複数消す、又は出す
    for (let i = 0; i < target_name_list.length; i++) {
      this.attachOrDetach(target_name_list[i], order);
    }
  }

  listAttachOrDetach(add_list, remove_list, order_0 = 0, order_1 = 1) {
    // 複数消 す出す
    this.multipleAtOrDt(add_list, order_0);
    this.multipleAtOrDt(remove_list, order_1);
  }

  loopAtOrDt(name, order = 0) {
    const target = document.querySelectorAll(name);
    for (let i = 0; i < target.length; i++) {
      switch (order) {
        case 0:
          target[i].classList.add("vanish");
          break;
        case 1:
          target[i].classList.remove("vanish");
          break;
        case 2:
          target[i].classList.toggle("vanish");
          break;
      }
    }
  }

  switchDisplay(loop_list, block_num) {
    // スイッチで動的に切り変える
    for (let i = 0; i < loop_list.length; i++) {
      if (i == block_num) {
        this.attachOrDetach(`${loop_list[i]}`, 1);
      } else {
        this.attachOrDetach(`${loop_list[i]}`, 0);
      }
    }
  }
}
// イベントを扱うクラス
class Event {
  eventTargetId(target, action, fc) {
    const element = getTarget(target);
    element.addEventListener(`${action}`, fc);
  }
  eventTargetAll(target, action, fc) {
    const elements = document.querySelectorAll(`${target}`);
    elements.forEach((element) => {
      element.addEventListener(`${action}`, fc);
    });
  }
  // 引数あり
  eventTarswitchDisplaygetNoName(target, action, fc, argument) {
    const element = document.getElementById(`${target}`);
    element.addEventListener(
      `${action}`,
      function () {
        fc(argument);
      },
      false
    );
  }
  eventTargetNoNameAll(target, action, fc, argument) {
    const elements = document.querySelectorAll(`${target}`);
    elements.forEach((element) => {
      element.addEventListener(
        `${action}`,
        function () {
          fc(argument);
        },
        false
      );
    });
  }
}

export function playSound(key) {
  // 鍵盤の音を鳴らす
  const audio = document.querySelector(`audio[data-key="${key}"]`);
  audio.currentTime = 0;
  audio.play();
}
export function listPlaySound(list) {
  let val = document.getElementById("vol_text");
  if (val.classList.contains("vanish")) {
    for (let i = 0; i < list.length; i++) {
      const audio = document.querySelector(`audio[data-key="${list[i]}"]`);
      audio.currentTime = 0;
      audio.play();
    }
  }
}

export function checkAndPlay(key) {
  // ボリュームがオンで音を鳴らす
  let val = document.getElementById("vol_text");
  if (val.classList.contains("vanish")) {
    playSound(key);
  }
}

export function getSoundRate(target) {
  // フォームから正解率を取得
  let result_list = document.getElementById(target);
  result_list = result_list.textContent;
  result_list = result_list.replace(/ /g, "");
  result_list = result_list.split("/");
  result_list[0] = result_list[0].replace(/\r?\n/g, "");
  result_list.sort();
  return result_list;
}
export let cs = new Css();

export function unDrowYellow(target) {
  //   黄色く塗られたのを全て戻す
  const keys = document.querySelectorAll(target);
  for (let i = 0; i < keys.length; i++) {
    let rr = keys[i].className;
    let color = com.juduge_w_or_b(rr);
    cs.cahgeCss(keys[i].id, `background-color: ${color}`);
  }
}

export let ev = new Event();
