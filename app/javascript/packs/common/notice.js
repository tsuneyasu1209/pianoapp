import {
  ev,
  cs,
  con,
} from "./common";
export class GetNotice {
  constructor() {
    try {
      this.text = this.getNothiceText();
    } catch (error) {
      this.text = "";
    }
    this.load_flag = this.judgeText();
    if (this.load_flag == 1) {
      this.showIcon(this.text)
      if (this.text != "") {
        setTimeout(function () {
          cs.attachOrDetach("insert_notice_message");
        }, 5000);
        ev.eventTargetId("insert_notice_message", "click", this.vanishMes);
      }
    }
  }
  getNothiceText() {
    // フラッシュに何か入っているか
    let text = document.getElementById("mali_send_mes");
    return text.textContent;
  }
  judgeText() {
    // テキストが入っているかでログインイベントを決める
    let a = document.getElementById("login_check").textContent
    return a

  }
  showIcon(text) {
    // メールマークを出す
    try {
      if (text.match(/メール/)) {
        cs.attachOrDetach(".mail_img", 1)
      }
    } catch (error) {}
  }

  vanishMes() {
    cs.attachOrDetach("insert_notice_message");
  }
}