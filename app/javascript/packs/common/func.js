import {
  cs,
  ev,
  con
} from "../common/common";
export class Vol {
  constructor() {
    ev.eventTargetId(".vol_block", "click", this.changeVolState);
  }
  changeVolState() {
    cs.attachOrDetach("vol_text", 2);
  }
}

class CommonFnc {
  // ○☓出す処理
  showJudgement(a) {
    function deleteJudgement(judgement) {
      judgement.style.cssText = "display: none";
    }
    // 正解の時の処理
    if (a == "ok") {
      cs.cahgeCss;
      const correct = document.getElementById("correct_modal");
      correct.style.cssText = "display: block";

      setTimeout(function () {
        deleteJudgement(correct);
      }, 300);
    }
    // 不正解の時の処理
    else if (a == "no") {
      const incorrect = document.getElementById("incorrect_modal");
      incorrect.style.cssText = "display: block";

      setTimeout(function () {
        deleteJudgement(incorrect);
      }, 300);
    }
  }

  juduge_w_or_b(params) {
    // 黒鍵白鍵色分け
    let color;
    if (params.match(/p/)) {
      color = "black";
    } else {
      color = "#FAF5E1";
    }
    return color;
  }

  splitList(use_list) {
    let maked_list = [];
    for (let i = 0; i < use_list.length; i++) {
      let base = use_list[i].split(",");
      maked_list.push(base[0], base[1], base[2]);
    }
    return maked_list;
  }
  changeSPToShrarp(params) {
    // #をpに変える♯
    if (params.match(/#/) || params.match(/♯/)) {
      let a = params.replace("#", "p");
      params = a;
    }
    return params;
  }

  changeShartToP(params, sharp = 0) {
    // ｐを＃に変える
    if (params.match(/p/)) {
      if (sharp == 0) {
        let a = params.replace("p", "#");
        params = a;
      } else {
        let a = params.replace("p", "♯");
        params = a;
      }
    }
    return params;
  }

  sliceQuestions(params) {
    // 登録している問題のリストに入れ直す
    const target = document.getElementById(`${params}`);
    let use_list = [];
    let name_list = target.value.split("-");
    for (let i = 0; i < name_list.length; i++) {
      let splied_list = name_list[i].split(":");
      use_list[i] = [splied_list[0], splied_list[1].split("/")];
    }
    return use_list;
  }

  insertSelectBoxContent(use_list, params) {
    // 問題をセレクトボックスにして挿入
    if (use_list.length != 0) {
      const target = document.getElementById(`${params}`);
      let insert_select_box =
        '<select class="cp_sl06" required><option value="" hidden disabled selected></option>';
      for (let i = 0; i < 3; i++) {
        try {
          insert_select_box += `<option value="${[i]}" class="sound_op">${
            use_list[i][0]
          }</option>`;
        } catch (TypeError) {
          insert_select_box += `<option value="${[
            i,
          ]}" class="sound_op">パターン${i}</option>`;
        }
      }
      insert_select_box += "</select>";
      target.insertAdjacentHTML("afterbegin", insert_select_box);
    } else {
      // 何もされていない時の処理
      // cahgeCss('sound_question_select_box_sentence', "display: block")
    }
  }
  createKeyList(base) {
    // baseに数字のリストを突っ込んでキーをの配列を作る
    let key_of_c = [2, 2, 1, 2, 2, 2, 1];
    for (let k = 1; k < 8; k++) {
      for (let i = 0; i < key_of_c.length; i++) {
        const last = parseInt(base.slice(-1)[0]);
        if (last > 87) {} else {
          base.push(last + key_of_c[i]);
        }
      }
    }
  }
  drowCodeKey(drow_key_num) {
    // 選択画面の鍵盤に色を塗る
    const targets = document.querySelectorAll(".code_block");
    for (let i = 0; i < targets.length; i++) {
      const elements = targets[i].children;
      for (let k = 0; k < drow_key_num[i].length; k++) {
        elements[drow_key_num[i][k]].style.backgroundColor = "yellow";
      }
    }
  }
  checUserLogin() {
    // ログインしているかどうか　0:してない 1:している
    let target = document.getElementById("login_check");
    if (target.textContent == "1") {
      return 1
    } else {
      return 0

    }
  }
  getScoreRateNon(target) {
    // フォームのnonの数を数える
    const targets = document.querySelectorAll(target);
    let non_nem = 0;
    for (let i = 0; i < targets.length; i++) {
      if (targets[i].value == "non") {
        non_nem++;
      }
    }
    return non_nem
  }
  showGameContent() {
    const target = document.getElementById("select_game");
    cs.attachOrDetach(".header_game_ul", 1);
    target.classList.toggle("li-in");
    target.classList.toggle("li-out");
    const target_tr = document.querySelector(".game_triangle");
    target_tr.classList.toggle("rota_up");
    target_tr.classList.toggle("rota_down");
    if (target_tr.classList.contains("rota_down")) {
      setTimeout(function () {
        cs.attachOrDetach(".header_game_ul");
      }, 1000);
    }
  }

  showMyPageContent() {
    const target = document.getElementById("select_mypage");
    cs.attachOrDetach(".header_mypage_ul", 1);
    target.classList.toggle("li-in");
    target.classList.toggle("li-out");
    const target_tr = document.querySelector(".mypage_triangle");
    target_tr.classList.toggle("rota_up");
    target_tr.classList.toggle("rota_down");
    if (target_tr.classList.contains("rota_down")) {
      setTimeout(function () {
        cs.attachOrDetach(".header_mypage_ul");
      }, 1000);
    }
  }

  showToolsContent() {
    const target = document.getElementById("select_tools");
    cs.attachOrDetach(".header_tools_ul", 1);
    target.classList.toggle("li-in");
    target.classList.toggle("li-out");
    const target_tr = document.querySelector(".tools_triangle");
    target_tr.classList.toggle("rota_up");
    target_tr.classList.toggle("rota_down");
    if (target_tr.classList.contains("rota_down")) {
      setTimeout(function () {
        cs.attachOrDetach(".header_tools_ul");
      }, 1000);
    }
  }
   changeHight(ele, ele_hig) {
    // 選択した項目で背景の高さをかえる
    const target = document.querySelector(ele);
    const header = document.getElementById("js_header").clientHeight
    const height = target.clientHeight + header + 30 //ヘッダーと中身の合計
    if (window.parent.screen.height < height) {
      // 背景の高さをかえる
        document.querySelector(ele_hig).style.height = 'auto';
    } else {
        document.querySelector(ele_hig).style.height = '100vh';
    }

}

}

export let com = new CommonFnc();