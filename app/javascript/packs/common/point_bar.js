
export class PointBar {
    constructor(login, target) {
        this.login = login
        if (this.login == 1) {
            this.ele = target
            this.point_bar = document.getElementById("point_bar")
            this.point_form_value = document.getElementById(this.ele).value
            this.insertPointBar()
            this.point_list = [this.point_form_value] //アニメーションの変数に使うリスト
            this.level = this.createLevel(this.point_form_value)
            this.insertLevel(this.level)
            this.progress_with = ""
        }
    }
    calculatePoint(judge, point) {
        // 獲得ポイントを計算して返す

        if (judge == "ok" && this.login == 1) {
            point += 10
        }
        return point
    }
    insertPointForm(point) {
        // 計算した値をフォームに挿入
        if (this.login == 1) {
            const target = document.getElementById(this.ele)
            const form_point = parseInt(target.value)
            const result = form_point + point
            target.value = result
        }
    }
    insertPointBar() {
        // フォームに予め入っている値をCSS変数に挿入
        const point_form_value = document.getElementById(this.ele).value
        const sliced_value = point_form_value.slice(-2)
        this.point_bar.style.setProperty("--width", sliced_value + "%")
    }
    createLevel(point) {
        // ポインの下二桁だけを切ってレベルを取得
        const point_form_value = point.toString()

        if (point_form_value.length >= 3) {
            return point_form_value.slice(0, -2);
        } else {
            return 0
        }
    }
    insertLevel(level) {
        // レベルを挿入
        let target = document.getElementById("insert_point")
        target.textContent = "Lv" + level
    }
    animationPropaty(point_list) {
        // アニメーションに使う値を挿入
        let target = document.getElementById("insert_point")
        target.classList.remove("bounce")
        let end = point_list.slice(-1)[0];
        let a = end.toString()
        end = a.slice(-2)
        if (end == "00") {
            let a = "1" + end
            end = a
            target.classList.add("bounce")
        }
        this.point_bar.style.setProperty("--width", end + "%")
    }
    levleBar(judge, point_list) {
        // レベルバーの最終的な処理
        if (this.login == 1) {
            let latest = parseInt(point_list.slice(-1)[0])
            point_list.push(this.calculatePoint(judge, latest))
            latest = parseInt(point_list.slice(-1)[0])
            this.animationPropaty(point_list)
            let level = this.createLevel(latest)
            this.insertLevel(level)
        }
    }
    getProgressWidth(question_times) {
        // 正解毎にどれぐらいバーを増やすのか
        let potion = 100 / question_times
        return potion
    }
    insertProgressBar(questions_count) {
        // 進行度のを更新する
        let width = questions_count * this.progress_with
        const progress_bar = document.getElementById("progress_bar")
        progress_bar.style.setProperty("--pro_width", width + "%")
    }
}