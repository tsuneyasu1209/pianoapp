import {
  con,
  cs
} from "./common";
import {
  com
} from "../common/func";
import {
  keyPlay
} from "../play/play"

// ２つのクラスで使う共通関数

// 結果出た後の聞けるようにする処理
function getTarget(params) {
  let selected_rate = params.target;
  let content_text = selected_rate.textContent;
  if (content_text.match(/#/)) {
    content_text = content_text.replace("#", "p");
  }
  content_text = content_text.split(":");
  return content_text;
}

function insertFormRate(
  insert_rate_sentence,
  insert_rate,
  target_edit,
  target_new
) {
  if (insert_rate.value != "non") {
    let update_rate = document.getElementById(target_edit);

    update_rate.value = insert_rate_sentence;
  } else if (insert_rate.value == "non") {
    let new_rate = document.getElementById(target_new);
    try {
      new_rate.value = insert_rate_sentence;
    } catch (error) {
      new_rate = document.getElementById(target_edit);
      new_rate.value = insert_rate_sentence;
    }
  } else {
    console.log("touroku");
  }
}

function sumRate(insert_rate, result_list) {
  // 計算に使う今までの正解率
  // フォームから確率を取得
  let insert_rate_sentence;
  if (result_list != "") {
    let calculation_num_base = insert_rate.value.split("/");

    // 計算に使うリスト
    let base_list = com.splitList(calculation_num_base);
    let search_list = com.splitList(result_list);

    // 挿入するリスト
    let insert_rate_list = [];
    for (let i = 0; i < search_list.length; i++) {
      if (i % 3 != 0) {
        continue;
      }
      // a, i +1回答数 ＋２正解率
      let index_num = base_list.indexOf(search_list[i]);
      if (index_num == -1) {
        if (i != 0) {
          let a = i - (i / 3) * 2;
          insert_rate_list.push(result_list[a]);
        } else {
          insert_rate_list.push(result_list[0]);
        }
      } else {
        // 前回と今回の確率の合算
        // 正解数の計算
        let now_correct_answer_rate =
          search_list[i + 2] / (100 / search_list[i + 1]);
        let ex_correct_answer_rate =
          base_list[index_num + 2] / (100 / base_list[index_num + 1]);

        // 正解数の合計
        let amount_of_anser_num =
          parseInt(now_correct_answer_rate) + parseInt(ex_correct_answer_rate);
        // 回答した問題の数の合計
        let amount_of_times_anser =
          parseInt(search_list[i + 1]) + parseInt(base_list[index_num + 1]);
        // 最終的な正解率
        let add_rate = Math.round(
          (amount_of_anser_num / amount_of_times_anser) * 100
        );
        // 出てきた問題を消去
        base_list.splice(index_num, 3);
        // 追加する文章
        let add_to_list_sentence = `${search_list[i]},${amount_of_times_anser},${add_rate}`;
        insert_rate_list.push(add_to_list_sentence);
      }
    }
    for (let i = 0; i < base_list.length; i++) {
      if (i % 3 != 0) {
        continue;
      }
      let add_to_insert_rate_list = `${base_list[i]},${base_list[i + 1]},${
        base_list[i + 2]
      }`;
      insert_rate_list.push(add_to_insert_rate_list);
    }
    insert_rate_sentence = insert_rate_list.join("/");
  } else {
    insert_rate_sentence = insert_rate.value;
  }
  return insert_rate_sentence;
}
// 結果計算のクラス
export class Calculate {
  // scoreとcodeの共通関数
  getFormValue(list) {
    // 引数全てのフォームの値を取得
    let target_list = [];
    let rate_list = [];
    for (let i = 0; i < list.length; i++) {
      let target = document.getElementById(list[i]);
      target_list.push(target);
      rate_list.push(target.value);
    }
    return [target_list, rate_list];
  }
  getInsertRate(rate_ob) {
    // オブジェクト形式をリストに形成する
    let list = [];
    for (let i in rate_ob) {
      let sentence = rate_ob[i].join("/");
      rate_ob[i] = sentence;
    }
    for (let i in rate_ob) {
      let sentence = [i, rate_ob[i]];
      list.push(sentence);
    }

    return list;
  }
  soreAndCodeFormInsert(target_key_list, result_rate_list) {
    let a = this.getFormValue(target_key_list);
    // // 確率の計算のために分解
    for (let i = 0; i < result_rate_list.length; i++) {
      let splited_result = result_rate_list[i][1].split("/");
      if (a[1][i] != "non") {
        let change_rate = sumRate(a[0][i], splited_result);
        result_rate_list[i][1] = change_rate;
      }
      insertFormRate(result_rate_list[i][1], a[0][i], target_key_list[i]);
    }
    a = this.getFormValue(target_key_list);
    for (let i = 0; i < result_rate_list.length; i++) {
      if (a[1][i] == "") {
        a[0][i].value = "non";
      }
    }
  }

  calculateCodeReusult(judge, object, key) {
    // コード用の確率計算
    if (judge == "ok") {
      object.push(`${key},1,100`);
    } else {
      object.push(`${key},1,0`);
    }
    this.lastCalculate(object);
  }


  firstCalculate(params, params2, params3) {
    // 正解率の計算
    let r_list = [];
    for (let q in params) {
      let search_key = q + "," + 1;
      for (let i = 0; i < params2.length; i++) {
        if (params2[i] == search_key) {
          r_list.push(search_key);
        }
      }
      let rate_reuslt = Math.round((r_list.length / params[q]) * 100);
      let result = q + "," + params[q] + "," + rate_reuslt;
      params3.push(result);
    }
  }
  lastCalculate(use_list) {
    // 前のと合わせる
    // リファクタリングの必要あり
    for (let i = 0; i < use_list.length; i++) {
      let base = use_list[i].split(",");
      for (let n = 0; n < use_list.length; n++) {
        let base2 = use_list[n].split(",");
        if (i != n && base[0] == base2[0]) {
          // 正解数
          let a_base = Math.round((base[1] * base[2]) / 100);
          let b_base = Math.round((base2[1] * base2[2]) / 100);
          // 出題回数
          let q_times = parseInt(base[1], 10) + parseInt(base2[1], 10);
          // 最終的な正解率
          let a_times = a_base + b_base;
          let ultimate_rates = Math.round((a_times / q_times) * 100);
          let ultimate_element = base[0] + "," + q_times + "," + ultimate_rates;
          // 計算したのを消去
          use_list.splice(n, 1);
          use_list[i] = ultimate_element;
        }
      }
    }
  }
  // 正解率の計算
  calculateCorrectAnswerRate(the_number_of_questions, result_array, params3) {
    if (times_of_anser == 1) {
      this.firstCalculate(the_number_of_questions, result_array, params3);
      this.lastCalculate(params3);
    }
  }
}
export class Show {
  getTargetCheckBox(ob) {
    // 使わなかったkeyを消す
    let target_list = [];
    for (let i in ob) {
      if (ob[i] == "") {
        let a = "va_" + i;
        target_list.push(a);
      }
    }
    cs.multipleAtOrDt(target_list);
    return target_list;
  }
  decideCss(target) {
    // 確率毎に色を振り分ける
    let class_state;
    if (parseInt(target) < 30) {
      class_state = "low_key";
    } else if (target > 69) {
      class_state = "hight_key";
    } else {
      class_state = "mid_key";
    }
    return class_state;
  }
  showRate() {
    let result;
    let class_state;
    result_list.sort();
    let insert_html = '<ul class="flex rate_block_ul">';
    for (let i = 0; i < result_list.length; i++) {
      result = result_list[i].split(",");
      class_state = this.decideCss(result[2]);

      // ＃の場合
      // result[0] = com.changeShartToP(result[0])

      insert_html += `<li class="sound_result ${class_state}">${result[0]}:${result[2]}%</li>`;
    }
    insert_html += "</ul>";
    const rate_block = document.querySelector(".rate_block");
    cs.cahgeCss(".score", "display: none");
    cs.cahgeCss(".rate", "display: block");

    rate_block.insertAdjacentHTML("afterbegin", insert_html);
    this.insertSoundRate(result_list);
  }
  showCodeRate(result_list, list) {
    // 確率を表示させる
    let use_list = result_list.split("/");
    let result;
    let code_result_list = document.querySelectorAll(".code_result_list");
    for (let i = 0; i < code_result_list.length; i++) {
      code_result_list[i].textContent = `${list[i]}:　`;
    }

    for (let i = 0; i < use_list.length; i++) {
      // htmlに描画
      result = use_list[i].split(",");
      let search_lndex = list.indexOf(result[0]);
      try {
        code_result_list[
          search_lndex
        ].textContent = `${result[0]}: ${result[2]}%`;
      } catch (error) {}
    }
  }

  // 確率を再計算してアップデート
  insertSoundRate(result_list) {
    let insert_rate_sentence = result_list.join("/");
    let insert_rate = document.querySelector(".sound_rate");
    // try {
    if (insert_rate.value != "non") {
      insert_rate_sentence = sumRate(insert_rate, result_list);
    }
    insertFormRate(insert_rate_sentence, insert_rate, "sound_rate_edit");
    // } catch (error) {
    //         console.log('error')
    //     }
  }
  selecteRate(e) {
    let content_text = getTarget(e);
    let key = [content_text[0]]
    keyPlay(key);
  }
  // レイトに乗せるとキーに色がつく
  dorwKeyColor(e) {
    let content_text = getTarget(e);
    let change_target_key = document.getElementById(`${content_text[0]}`);
    change_target_key.style.backgroundColor = "yellow";
  }
  deleteKeyColor(e) {
    let content_text = getTarget(e);
    let change_target_key = document.getElementById(`${content_text[0]}`);
    if (content_text[0].match(/p/)) {
      change_target_key.style.backgroundColor = "#000000";
    } else {
      change_target_key.style.backgroundColor = "#FAF5E1";
    }
  }
  sortRate(result_list) {
    // 正解率からの振り分け
    let use_result_list = com.splitList(result_list);
    let keys = document.querySelectorAll(".key");
    // 検索用のidリスト
    let keys_id_list = [];
    for (let i = 0; i < keys.length; i++) {
      keys_id_list.push(keys[i].id);
      let target = document.getElementById(keys[i].id);
      target.textContent = "";
    }
    for (let i = 0; i < use_result_list.length; i++) {
      if (i % 3 != 0) {
        continue;
      }
      let matched_key_num = keys_id_list.indexOf(use_result_list[i]);
      try {
        let target_key = document.getElementById(`${keys[matched_key_num].id}`);
        let class_state;
        let rate_content;
        if (parseInt(use_result_list[i + 2]) < 30) {
          class_state = "low_key";
          rate_content = "☓";
        } else if (
          parseInt(use_result_list[i + 2]) > 29 &&
          parseInt(use_result_list[i + 2]) < 60
        ) {
          class_state = "mid_key";
          rate_content = "△";
        } else if (parseInt(use_result_list[i + 2]) > 89) {
          class_state = "hight_key";
          rate_content = "◎";
        } else {
          class_state = "hight_key";
          rate_content = "○";
        }
        // 描画する前に元に戻す
        let class_list = ["low_key", "mid_key", "hight_key"];
        for (let i = 0; i < class_list.length; i++) {
          if (target_key.classList.contains(class_list[i])) {
            target_key.classList.remove(class_list[i]);
          }
        }
        target_key.classList.add(`${class_state}`);
        target_key.textContent = `${rate_content}`;
      } catch (error) {
        //   // attachOrDetach("show_sound_rate", 0);
      }
    }
  }
  getRateOnlyName(result_list) {
    // 詳細な確率を表示するのに使うリストを返す
    let result_list_only_key = [];
    for (let i = 0; i < result_list.length; i++) {
      let splied_list = result_list[i].split(",");
      result_list_only_key.push(splied_list[0]);
    }
    return result_list_only_key;
  }

  clickShowRate(e) {
    // 選んだ鍵盤に正解率が記録されているか
    if (now_display >= 0 || check_key_flag == 1) {
      let insert_dom_list = [
        ".rate_table_selected_key",
        ".rate_table_qestion_times",
        ".rate_table_rate",
      ];
      let target = e.target.id;
      let key_index;
      if (now_display == 1) {
        try {
          key_index = result_list_only_key.indexOf(target); //glo
        } catch (error) {
          key_index = -1;
        }
      } else {
        key_index = result_list_only_key.indexOf(target);
      }
      if (key_index != -1) {
        // 入っている場合
        let splied_list = result_list[key_index].split(","); //glo
        splied_list[0] = com.changeShartToP(splied_list[0]);
        splied_list[1] += "回";
        splied_list[2] += "%";
        for (let i = 0; i < insert_dom_list.length; i++) {
          const target = document.querySelector(`${insert_dom_list[i]}`);
          target.textContent = splied_list[i];
        }
      } else {
        let none_list = [target, "0回", "0%"];
        none_list[0] = com.changeShartToP(none_list[0]);
        // let error_code = error_code_target.textContentgeShartToP(none_list[0]);
        for (let i = 0; i < insert_dom_list.length; i++) {
          const target = document.querySelector(`${insert_dom_list[i]}`);
          target.textContent = none_list[i];
        }
      }
    }
  }
}

export let cal = new Calculate();
export let show = new Show();