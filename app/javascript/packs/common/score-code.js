import { show } from "./rate";
export class GetSelect {
  constructor(target) {
    this.all_select = this.getCheckedValu(target);
    this.question_times = this.getQuestionTimes();
  }
  getQuestionTimes() {
    // 問題を何回繰り返すかの取得
    let question_times = 0;
    const question_score_num = document.getElementsByName("question_score_num");
    for (let i = 0; i < question_score_num.length; i++) {
      if (question_score_num[i].checked) {
        question_times = question_score_num[i].value;
      }
    }
    return question_times;
  }

  getCheckedValu(target) {
    // チェックされいる要素のidをリストで返す
    const all_check_key = document.querySelectorAll(target);
    let checked_value_list = [];
    for (let i = 0; i < all_check_key.length; i++) {
      if (all_check_key[i].checked) {
        checked_value_list.push(all_check_key[i].id);
      }
    }
    return checked_value_list;
  }
}

