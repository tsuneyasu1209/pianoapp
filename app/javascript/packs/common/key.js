import { cs } from "../common/common";
// 鍵盤を扱うクラス
class Key {
  swithOctave(e) {
    let target = e.target.id;
    if (target == "oc_s") {
      octave = 0;
    } else {
      octave = 1;
    }
  }
  singeKeyAnser(octave, target, anser) {
    // シングルオクターブの時の挙動
    let joined_anser = target;
    if (octave == 0) {
      let a = target.split("_");
      let anser_num = anser.slice(-1);
      joined_anser = a[1] + anser_num;
    }
    return joined_anser;
  }
  ratePageKeyBtns() {
    // 問題が終わった後の鍵盤の状態を整える
    const add_list = [".button_block_center", ".single_key", ".button_block"];
    const remove_list = [".button_block_box_unde", ".insert_key"];
    cs.listAttachOrDetach(add_list, remove_list);
    const target = document.getElementById("all_btn");
    target.classList.add("all_on");
    let target_block_list = [
      "A0",
      "C1",
      "C2",
      "C3",
      "C4",
      "C5",
      "C6",
      "C7",
      "C8",
    ];

    for (let i = 0; i < target_block_list.length; i++) {
      let target_btn = document.getElementById(`${target_block_list[i]}_btn`);
      target_btn.classList.add("all_on");
      let change_btn = document.querySelector(`.${target_block_list[i]}_block`);
      change_btn.classList.remove("vanish");
    }
  }
}
export let keyEv = new Key();
