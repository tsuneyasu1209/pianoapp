import {
  con
} from "./common";
import {
  keyPlay
} from "../play/play"
// 音の数だけ問題を制作
export function make_questions_selected_times(registed_question = "") {
  let question_array = [];
  let checked_selected_sound_num;
  // 音の数の取得
  const selected_sound_nums = document.getElementsByName("selected_sound_num");
  selected_sound_nums.forEach((selected_sound_num) => {
    if (selected_sound_num.checked) {
      checked_selected_sound_num = selected_sound_num.value;
    }
  });
  if (registed_question == "") {
    for (let i = 0; i < checked_selected_sound_num; i++) {
      question_array.push(createQuestion());
    }
  } else {
    for (let i = 0; i < checked_selected_sound_num; i++) {
      let random_num = Math.floor(Math.random() * registed_question.length);
      question_array.push(registed_question[random_num]);
    }
  }
  console.log(question_array);
  return question_array;
}
// 問題を作る
export function createQuestion() {
  let key_code_array = ["a", "b", "c", "d", "e", "f", "g"];
  let key_code_array_sharp = [
    "a",
    "b",
    "c",
    "d",
    "e",
    "f",
    "g",
    "cp",
    "dp",
    "fp",
    "gp",
    "ap",
  ];
  let key_code_array_a_to_b = ["a", "b"];
  let key_code_array_a_to_b_sharp = ["a", "b", "ap"];
  let num_array_random = Math.floor(Math.random() * num_array.length);
  let number_of_key = num_array[num_array_random];
  let ckecked_key_value;
  // 黒鍵を入れるか入れないか
  const selected_keys = document.getElementsByName("selected_key");
  selected_keys.forEach((selected_key) => {
    if (selected_key.checked) {
      ckecked_key_value = selected_key.value;
    }
  });
  // A0の場合
  if (number_of_key == 0) {
    if (ckecked_key_value == "both") {
      let key_random = Math.floor(Math.random() * 3);
      let key_code_of_question = key_code_array_a_to_b_sharp[key_random];
      let question = key_code_of_question + number_of_key;
      return question;
    } else {
      let key_random = Math.floor(Math.random() * 2);
      let key_code_of_question = key_code_array_a_to_b[key_random];
      let question = key_code_of_question + number_of_key;
      return question;
    }
    // C8の場合
  } else if (number_of_key == 8) {
    let question = "c8";
    return question;
  } else {
    // ドレミ決める
    if (ckecked_key_value == "both") {
      let key_random = Math.floor(Math.random() * 12);
      // 音階を決める
      let key_code_of_question = key_code_array_sharp[key_random];
      let question = key_code_of_question + number_of_key;
      return question;
    } else {
      let key_random = Math.floor(Math.random() * 7);
      // 音階を決める
      let key_code_of_question = key_code_array[key_random];
      let question = key_code_of_question + number_of_key;
      return question;
    }
  }
}

// タイマーの設定
export function questionsSoundPlay(questions) {
  let playQuestion = function () {
    let key = [questions[i]]
    keyPlay(key);
    i++;
  };
  let tm = 1000;
  let i = 0;
  let tid = setInterval(function () {
    playQuestion();
    if (i >= questions.length) {
      clearInterval(tid);
    }
  }, tm);
}