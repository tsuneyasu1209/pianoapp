import {
  ev
} from "../common/common";

class Opning {
  constructor() {
    this.getUserInfo();
    this.drowError();
  }
  getUserInfo() {
    //   登録された名前とemailを挿入
    let targets = document.querySelectorAll(".contact_user_info");
    let form = document.querySelectorAll(".form");
    for (let i = 0; i < targets.length; i++) {
      form[i].value = targets[i].textContent;
    }
  }
  drowError() {
    //   空欄のフォームを赤く塗る
    let targets = document.querySelectorAll(".contact_form");
    for (let i = 0; i < targets.length; i++) {
      if (targets[i].value == "") {
        targets[i].style.border = "2px solid red";
      } else {
        targets[i].style.border = "1px solid rgb(195, 195, 195)";
      }
    }
  }
}
let contact = new Opning();
ev.eventTargetAll(".contact_form", "change", contact.drowError);