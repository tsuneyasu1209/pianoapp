import {
  unDrowYellow,
  con,
  ev,
  cs
} from "../common/common";
import {
  com,
  Vol
} from "../common/func";
import {
  cal,
  show
} from "../common/rate";
import {
  ck
} from "../common/checbox";
import {
  GetSelect
} from "../common/score-code";
import {
  PointBar
} from "../common/point_bar";

import {
  play_code,
  loadAllKey
} from "../play/play"
let all = loadAllKey()

// 変数
let check_key_flag = 0; //0: 選択画面 1: 確率画面
let times_of_anser = 1; //回答数
let user_select; //選択した値が入るクラス :all_select :question_times
let check_key_count; //チェックボックスのチェックス数
let selected_key_num;
let question; //問題を入れる
let question_coount = 1; //現在の問題数
let anser_list = []; //答え入れるリスと
let result_storage_ob; //正解率を入れるOB
let result_rate_list; //OBからリストに 0:key_name_o 1:確率
let point = 0 //ポイント
let login = com.checUserLogin(); // ログイン　0:してない 1:している
let point_bar = new PointBar(login, "point_code_form_edit")
new Vol();
// 開幕の処理を扱うクラス
class Opening {
  constructor() {

    this.target_key_list = [
      "code_rate_edit_M",
      "code_rate_edit_M7",
      "code_rate_edit_m",
      "code_rate_edit_m7",
      "code_rate_edit_m7_5",
      "code_rate_edit_sus4",
      "code_rate_edit_dim7",
      "code_rate_edit_aug",
      "code_rate_edit_6",
      "code_rate_edit_add9",
      "code_rate_edit_7",
    ];


    this.drow_key_num = [
      [0, 4, 7],
      [0, 4, 7, 11],
      [0, 3, 7],
      [0, 3, 7, 10],
      [0, 3, 6, 10],
      [0, 5, 7],
      [0, 3, 6, 9],
      [0, 4, 8],
      [0, 4, 7, 9],
      [0, 4, 7, 14],
      [0, 4, 7, 10]
    ]
    com.drowCodeKey(this.drow_key_num);
    this.result_storage_ob = this.createVoidOb();
  }

  createVoidOb() {
    let result_storage_ob = {};
    for (let i = 0; i < 11; i++) {
      result_storage_ob[`ckey_num_${i}`] = [];
    }
    return result_storage_ob;
  }

}

let op = new Opening();
result_storage_ob = op["result_storage_ob"];

// 選択画面で使うクラス
class Select {
  codeeQuestionAllBtn(e) {
    // チェックボックsを全て選択、又ははずす
    const target = e.target;
    let check_list = [".check_code"];
    ck.allCheckOrUncheck(check_list, target);
  }

  varidateCheckedKey(e) {
    const target = e.target;
    // 一つしか選択出来ないようにする
    function switchKeyCheckBox(target) {
      if (target.classList.contains("check_code")) {
        ck.changeChecked(".check_code", target);
      }
    }
    ck.checkUnckeck(".validate_check");
    if (login == 0) {
      switchKeyCheckBox(target);
    }
    if (check_key_flag == 1) {
      // 正解率の画面の時
      let a = target.id.split("_");
      selected_key_num = a[2];

      switchKeyCheckBox(target);
      show.showCodeRate(result_storage_ob[target.id], question["key_name"]);
    }
  }
}

let sel = new Select();

// スタートボタンを押した時の挙動

// 問題を作るクラス
class MakeQuestion {
  constructor(question_num = question_coount) {
    this.key_name = [
      "C",
      "C♯",
      "D",
      "D♯",
      "E",
      "F",
      "F♯",
      "G",
      "G♯",
      "A",
      "A♯",
      "B",
    ];

    this.code_list = this.cutSelectNum();
    this.question = this.createQuestion();
    cs.insertQuestionsNum("code_question_num", question_num);
  }
  cutSelectNum() {
    //   リストに選んだコードのタイトル番号を入れる
    let num_list = [];
    for (let i = 0; i < user_select["all_select"].length; i++) {
      let code_ob = {};
      const target = document.getElementById(`${user_select["all_select"][i]}`)
        .name;
      let a = user_select["all_select"][i].split("_");
      code_ob[`${target}`] = a[2];
      num_list.push(code_ob);
    }
    return num_list;
  }

  createQuestion() {
    let min = 1;
    let max = 12;
    let question_name;
    let anser;
    let key_code_num;
    // 鍵盤の乱数
    let random_key_num = Math.floor(Math.random() * (max + 1 - min)) + min;
    // コードの乱数
    let random_code_num = Math.floor(Math.random() * this.code_list.length);
    // オブジェクトから、コード名とリストのイデックスを取り出す
    for (let city in this.code_list[random_code_num]) {
      question_name = this.key_name[random_key_num - 1] + city;
      let num = this.code_list[random_code_num][city];
      key_code_num = "ckey_num_" + num;
      num = parseInt(num);
      let code = [play_code.plus_num_list[num]];
      anser = play_code.calCodeNum(code, random_key_num);
    }
    this.insertQuestion(question_name);

    return [anser, key_code_num, this.key_name[random_key_num - 1]]; //0:答え 1:確率計算に使う 2:key
  }
  insertQuestion(sentece) {
    let target = document.getElementById("insert_code_question");
    target.textContent = sentece;
  }
}

// 答えに使うクラス
class Anser {
  constructor(target) {
    this.dorwYellow(target);
    this.key = this.sliceKeyId(target);
  }
  dorwYellow(target) {
    // 選んだキーに色を塗る
    let target_key = document.getElementById(target);
    target_key.style.backgroundColor = "yellow";
  }

  sliceKeyId(target) {
    //   答え合わせようにIDを切り取る
    let a = target.split("_");
    return a[2];
  }
}

// 正解のジャッジに使うクラス
class CheckAnser {
  constructor(question_array, anser_array) {
    this.judge = this.checkAnser(question_array, anser_array);
    com.showJudgement(this.judge);
  }
  checkAnser(question_array, anser_array) {
    let judge;
    let corrct_count = 0;
    // 正解の数を数える
    let question_copy_list = question_array.slice();
    for (let i = 0; i < question_array.length; i++) {
      let num = parseInt(anser_array[i]);
      let a = question_copy_list.indexOf(num);
      if (a != -1) {
        corrct_count++;
        question_copy_list[a] = "";
      }
    }
    if (corrct_count == question_array.length) {
      judge = "ok";
    } else {
      judge = "no";
    }
    setTimeout(function () {
      unDrowYellow(".key");
    }, 300);

    return judge;
  }
}

class CodeRate {
  constructor(
    que = question["question"][0][0], //問題
    ans = anser_list, //答え
    str = result_storage_ob[`${question["question"][1]}`], //入れる配列
    name = question["question"][2] //key
  ) {
    let judge = new CheckAnser(que, ans); // 答えがあっているかチェック
    if (times_of_anser == 1) {
      cal.calculateCodeReusult(judge["judge"], str, name);
      point = point_bar.calculatePoint(judge["judge"], point)
      point_bar.levleBar(judge["judge"], point_bar["point_list"]) //レベルバーの更新
    }
    return judge;
  }
}
class AfterJudge {
  constructor(judge) {
    if (judge == "ok") {
      point_bar.insertProgressBar(question_coount) //進行度のバーを更新
      question_coount++;
      question = new MakeQuestion(); //新しい問題を作成
      times_of_anser = 1;
    } else {
      times_of_anser++;
    }
    play_code.playCode(anser_list);
    anser_list = []; //答えを初期化
    return question;
  }
}

class AfterAnser {
  constructor() {
    // 指定の問題数をこなしたら
    if (
      user_select["question_times"] < question_coount
      // 1 < question_coount
    ) {
      show.getTargetCheckBox(result_storage_ob);
      const del_list = [
        ".code_question_block",
        ".select_h_t",
        "score_question_btn_block",
      ];
      const ap_list = [".select_code_block", ".code_rate_block"];
      cs.listAttachOrDetach(del_list, ap_list); // 画面切り替え
      ck.allClreCheckBox(".check_code"); //チェックボックのチェックを外す
      result_rate_list = cal.getInsertRate(result_storage_ob);
      if (login == 1) {
        cal.soreAndCodeFormInsert(op["target_key_list"], result_rate_list);
        point_bar.insertPointForm(point)
      }
      ev.eventTargetAll(".code_result_list", "click", this.selectRate);
    }
  }
  selectRate(e) {
    // 確率を押すと鍵盤に描画される
    unDrowYellow(".key");
    let anser;
    try {
      const a = e.target.id.split("_");
      const key_num = a[1];
      let num = parseInt(key_num);
      let code = [play_code.plus_num_list[selected_key_num]];
      anser = play_code.calCodeNum(code, num);
      cs.attachOrDetach(".check_code_error");
      for (let i = 0; i < anser[0].length; i++) {
        cs.cahgeCss(`code_key_${anser[0][i]}`, "background-color: yellow");
      }
      play_code.playCode(anser[0]);
    } catch (error) {
      cs.attachOrDetach(".check_code_error", 1);
    }
  }
}
// スタートボタンでスタート
function codeGameStart() {
  check_key_count = ck.checkError(".check_code");
  user_select = new GetSelect(".check_code");
  question = new MakeQuestion();
  if (check_key_count != 0) {
    //一つ以上選択している
    const a_l = [".select_code_block", ".check_code_error"];
    const b_l = [".code_question_block", "anser_code_key"];
    cs.listAttachOrDetach(a_l, b_l);
    check_key_flag = 1; //0: 選択画面 1: 確率画面
    point_bar["progress_with"] = point_bar.getProgressWidth(user_select["question_times"])

  }
}
// 実際のプレイング
function selectCodeAnser(e) {
  const target = e.target.id;
  let an = new Anser(target); //idを切り取って鍵盤の色を消す
  anser_list.push(an["key"]);
  if (question["question"][0][0].length == anser_list.length) {
    let judge = new CodeRate(); //確率計算
    new AfterJudge(judge["judge"]); //新し問題を作る
    new AfterAnser(); //指定の回数問題をこなしたら
  }
}
// チェックボックsを全て選択、又ははずす
ev.eventTargetAll(".all_check_or_uncheck", "click", sel.codeeQuestionAllBtn);
// 正解率の画面では一つしか選択出来ないようにする
ev.eventTargetAll(".validate_check", "click", sel.varidateCheckedKey);
// スタートボタン処理
ev.eventTargetId("score_start_btn", "click", codeGameStart);
ev.eventTargetAll(".key", "click", selectCodeAnser);