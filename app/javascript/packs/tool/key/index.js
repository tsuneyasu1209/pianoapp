import {
    ev,
    unDrowYellow
} from "../../common/common"
let top_key_list = [
    [0, 3, 6, 7, 10, 13, 16],
    [0, 3, 6, 8, 10, 13, 16],
    [1, 3, 6, 8, 10, 13, 16],
    [1, 3, 6, 8, 11, 13, 16],
    [1, 4, 6, 8, 11, 13, 16],
    [1, 4, 6, 8, 11, 14, 16],
    [1, 4, 7, 8, 11, 14, 16],
    [1, 4, 7, 8, 11, 14, 0],
    [0, 3, 6, 7, 10, 13, 15],
    [0, 3, 5, 7, 10, 13, 15],
    [0, 3, 5, 7, 10, 12, 15],
    [0, 2, 5, 7, 10, 12, 15],
    [0, 2, 5, 7, 9, 12, 15],
    [16, 2, 5, 7, 9, 12, 15],
    [16, 2, 5, 6, 9, 12, 15],
]
class Score {
    constructor() {

        ev.eventTargetAll(".check_key", "click", this.onlyOneCheck)
        ev.eventTargetAll(".check_key", "click", this.showUseKey)
    }
    onlyOneCheck(e) {
        let targets = document.querySelectorAll(".check_key")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].id != e.target.id) {
                targets[i].checked = false
            }
        }
    }

    showUseKey(e) {
        let splited = e.target.id.split("_")
        let num = parseInt(splited[2])
        let targets = document.querySelectorAll(".js_top_key")
        for (let i = 0; i < targets.length; i++) {
            targets[i].classList.add("vanish")
        }
        unDrowYellow(".key")
        for (let i = 0; i < top_key_list[num].length; i++) {
            targets[top_key_list[num][i]].classList.remove("vanish")
            let a = ".to_k_" + top_key_list[num][i]
            document.querySelector(a).style.backgroundColor = "yellow";
        }
    }
}
new Score()