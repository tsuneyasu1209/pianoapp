import {
    ev,
    con,
    cs
} from "../../common/common"
import {
    ck
} from "../../common/checbox"


class Opning {
    constructor() {
        this.drow_key_num = [
            [0, 4, 7],
            [0, 4, 7, 11],
            [0, 3, 7],
            [0, 3, 7, 10],
            [0, 3, 6, 10],
            [0, 5, 7],
            [0, 3, 6, 9],
            [0, 4, 8],
            [0, 4, 7, 9],
            [0, 4, 7, 14],
            [0, 4, 7, 10]
        ]
        this.code = [
            "M",
            "M7",
            "m",
            "m7",
            "m7-5",
            "sus4",
            "dim7",
            "aug",
            "6",
            "add9",
            "7"
        ]
        this.key_name = [
            "C",
            "C♯",
            "D",
            "D♯",
            "E",
            "F",
            "F♯",
            "G",
            "G♯",
            "A",
            "A♯",
            "B",
        ];
    }
}
const op = new Opning()
class Select {

    codeeQuestionAllBtn(e) {
        // チェックボックsを全て選択、又ははずす
        const target = e.target;
        let check_list = [".check_code"];
        ck.allCheckOrUncheck(check_list, target);
    }
    validateCheck() {
        // チェック数で全部消すつけるをかえる
        ck.checkUnckeck(".validate_check");
    }

}

class Generate {
    constructor() {
        this.generate_list_ = [] //出力するリスト
        this.getGenerateList() //どのコードを使うか
        this.code_list = []
        this.times_ = this.getGenerateTimes()
        for (let i = 0; i < this.times_; i++) {
            const code_num = this.makeInt(this.generate_list_.length - 1)
            let a = op["key_name"][this.makeInt(11)] + op["code"][code_num]
            this.code_list.push(a)
        }
        return this.code_list
    }
    makeInt(int) {
        // ランダムな数を返す
        const min = 0;
        const max = int
        const a = Math.floor(Math.random() * (max + 1 - min)) + min;
        return a
    }
    getGenerateList() {
        // どのコードを使うか
        const targets = document.querySelectorAll(".check_code")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].checked == true) {
                this.generate_list_.push(i)
            }
        }
    }
    getGenerateTimes() {
        // 何個出題するか
        const targets = document.querySelectorAll(".sound_select")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].checked == true) {
                return parseInt(targets[i].value)
            }
        }
    }
}

class Show {
    constructor(code_list) {
        this.makeHtml(code_list)
    }
    makeHtml(code_list) {
        const insert_generate = document.querySelectorAll(".insert_generate")
        const targets = document.querySelectorAll(".insert_code")
        for (let i = 0; i < code_list.length; i++) {
            targets[i].textContent = code_list[i]
            switch (i) {
                case 0:
                    insert_generate[0].classList.remove("vanish")
                    insert_generate[1].classList.remove("vanish")
                    break;
                case 20:
                    insert_generate[2].classList.remove("vanish")
                    insert_generate[3].classList.remove("vanish")
                    insert_generate[4].classList.remove("vanish")
                    insert_generate[5].classList.remove("vanish")
                    break;
                case 40:
                    insert_generate[6].classList.remove("vanish")
                    insert_generate[7].classList.remove("vanish")
                    insert_generate[8].classList.remove("vanish")
                    insert_generate[9].classList.remove("vanish")
                    break;

                default:
                    break;
            }
        }
    }
}

let sel = new Select();

function start() {
    // スタートボタン
    const gene = new Generate()
    new Show(gene)
    cs.attachOrDetach("code_generate_select_section")
    cs.attachOrDetach("code_generate_section", 1)
}

function restart() {
    // スタートボタン
    const gene = new Generate()
    new Show(gene)
}

function resselect() {
    // スタートボタン
    cs.attachOrDetach("code_generate_select_section", 1)
    cs.attachOrDetach("code_generate_section")
}

ev.eventTargetAll(".all_check_or_uncheck", "click", sel.codeeQuestionAllBtn);
ev.eventTargetAll(".validate_check", "click", sel.validateCheck);
ev.eventTargetId("score_start_btn", "click", start)
ev.eventTargetId("generate_restart", "click", restart)
ev.eventTargetId("generate_reselect", "click", resselect)