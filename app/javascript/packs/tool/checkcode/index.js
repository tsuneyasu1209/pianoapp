import {
    ev,
    con
} from "../../common/common"
import {
    play_code,
    loadAllKey
} from "../../play/play"

let all = loadAllKey()

// let skip_list = [0]
// let num = 0
// for (let i = 0; i < 10; i++) {
//     skip_list.push(num += 27)
// }

// 塗るキーを計算
// let dorw_list = []
// for (let i = 0; i < drow_key_num.length; i++) {
//     for (let k = 0; k < drow_key_num[i].length; k++) {
//         let a = drow_key_num[i][k] + skip_list[i]
//         let b = a + key_num
//         dorw_list.push(b)
//     }

// }


let dorw_list = [
    [0, 4, 7, 27, 31, 34, 38, 54, 57, 61, 81, 84, 88, 91, 108, 111, 114, 118, 135, 140, 142, 162, 165, 168, 171, 189, 193, 197, 216, 220, 223, 225, 243, 247, 250, 257, 270, 274, 277, 280],
    [1, 5, 8, 28, 32, 35, 39, 55, 58, 62, 82, 85, 89, 92, 109, 112, 115, 119, 136, 141, 143, 163, 166, 169, 172, 190, 194, 198, 217, 221, 224, 226, 244, 248, 251, 258, 271, 275, 278, 281],
    [2, 6, 9, 29, 33, 36, 40, 56, 59, 63, 83, 86, 90, 93, 110, 113, 116, 120, 137, 142, 144, 164, 167, 170, 173, 191, 195, 199, 218, 222, 225, 227, 245, 249, 252, 259, 272, 276, 279, 282],
    [3, 7, 10, 30, 34, 37, 41, 57, 60, 64, 84, 87, 91, 94, 111, 114, 117, 121, 138, 143, 145, 165, 168, 171, 174, 192, 196, 200, 219, 223, 226, 228, 246, 250, 253, 260, 273, 277, 280, 283],
    [4, 8, 11, 31, 35, 38, 42, 58, 61, 65, 85, 88, 92, 95, 112, 115, 118, 122, 139, 144, 146, 166, 169, 172, 175, 193, 197, 201, 220, 224, 227, 229, 247, 251, 254, 261, 274, 278, 281, 284],
    [5, 9, 12, 32, 36, 39, 43, 59, 62, 66, 86, 89, 93, 96, 113, 116, 119, 123, 140, 145, 147, 167, 170, 173, 176, 194, 198, 202, 221, 225, 228, 230, 248, 252, 255, 262, 275, 279, 282, 285],
    [6, 10, 13, 33, 37, 40, 44, 60, 63, 67, 87, 90, 94, 97, 114, 117, 120, 124, 141, 146, 148, 168, 171, 174, 177, 195, 199, 203, 222, 226, 229, 231, 249, 253, 256, 263, 276, 280, 283, 286],
    [7, 11, 14, 34, 38, 41, 45, 61, 64, 68, 88, 91, 95, 98, 115, 118, 121, 125, 142, 147, 149, 169, 172, 175, 178, 196, 200, 204, 223, 227, 230, 232, 250, 254, 257, 264, 277, 281, 284, 287],
    [8, 12, 15, 35, 39, 42, 46, 62, 65, 69, 89, 92, 96, 99, 116, 119, 122, 126, 143, 148, 150, 170, 173, 176, 179, 197, 201, 205, 224, 228, 231, 233, 251, 255, 258, 265, 278, 282, 285, 288],
    [9, 13, 16, 36, 40, 43, 47, 63, 66, 70, 90, 93, 97, 100, 117, 120, 123, 127, 144, 149, 151, 171, 174, 177, 180, 198, 202, 206, 225, 229, 232, 234, 252, 256, 259, 266, 279, 283, 286, 289],
    [10, 14, 17, 37, 41, 44, 48, 64, 67, 71, 91, 94, 98, 101, 118, 121, 124, 128, 145, 150, 152, 172, 175, 178, 181, 199, 203, 207, 226, 230, 233, 235, 253, 257, 260, 267, 280, 284, 287, 290],
    [11, 15, 18, 38, 42, 45, 49, 65, 68, 72, 92, 95, 99, 102, 119, 122, 125, 129, 146, 151, 153, 173, 176, 179, 182, 200, 204, 208, 227, 231, 234, 236, 254, 258, 261, 268, 281, 285, 288, 291]

]

const del = document.querySelectorAll(".js_select_code")
const key = document.querySelectorAll(".code_key")


export class TopCode {
    constructor() {
        ev.eventTargetAll(".js_select_code", "click", this.selectCode)
        ev.eventTargetAll(".js_code_play_btn", "click", this.playCode)
    }
    selectCode(e) {
        let num = e.target.dataset.code
        let key_num = parseInt(num)
        for (let i = 0; i < key.length; i++) {
            // 鍵盤を元に戻す
            key[i].classList.remove("js_drow_check_code")

        }

        for (let i = 0; i < dorw_list[key_num].length; i++) {
            // 色塗り
            key[dorw_list[key_num][i]].classList.add("js_drow_check_code")

        }

        const target = e.target
        for (let i = 0; i < del.length; i++) {
            // 選んだキーを白くする
            del[i].classList.remove("top_select_code")
        }
        target.classList.add("top_select_code")
    }
    playCode(e) {

        let data_num = e.target.dataset.codeNumber
        let code = [play_code.plus_num_list[data_num]];

        let num
        for (let i = 0; i < del.length; i++) {
            if (del[i].classList.contains("top_select_code")) {
                const a = del[i].id.split("_");
                const key_num = a[1];
                num = parseInt(key_num);
                break
            }

        }
        let anser = play_code.calCodeNum(code, num)
        play_code.playCode(anser[0]);

    }

}
new TopCode()
