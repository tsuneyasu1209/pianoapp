import {
    con
} from "../../common/common";

export class Calendar {
    constructor() {
        const vanish_targets = document.getElementById("calendar").childNodes;
        for (let i = 0; i < vanish_targets.length; i++) {
            vanish_targets[i].remove();
        }
        this.weeks_ = ["日", "月", "火", "水", "木", "金", "土"];
        const date = new Date();
        this.year_ = date.getFullYear();
        this.month_ = date.getMonth() + 1;
        this.day_ = date.getDate()
        let today = this.year_ + "/" + this.month_ + "/" + this.day_
        document.getElementById("insert_select_date").textContent = today;
        this.showCalendar();
    }
    checkCal(num) {
        // 3600で一時間
        let add_class;

        if (num != "") {
            num = parseInt(num);
            if (num <= 3600) {
                //     // 一時間以下
                add_class = "gre4";
            } else if (num > 3600 && num <= 5400) {
                // 一時間以上二時間半以下
                add_class = "gre3";
            } else if (num > 5400 && num <= 7200) {
                // 二時間半以上四時間以下
                add_class = "gre2";
            } else {
                add_class = "gre1";
            }
        }
        return add_class;
    }
    createCalendar(year, month) {
        const startDate = new Date(year, month - 1, 1); // 月の最初の日を取得
        const endDate = new Date(year, month, 0); // 月の最後の日を取得
        const endDayCount = endDate.getDate(); // 月の末日
        const lastMonthEndDate = new Date(year, month - 1, 0); // 前月の最後の日の情報
        const lastMonthendDayCount = lastMonthEndDate.getDate(); // 前月の末日
        const startDay = startDate.getDay(); // 月の最初の日の曜日を取得
        let dayCount = 1; // 日にちのカウント
        let calendarHtml = ""; // HTMLを組み立てる変数
        const inset_cal_text = document.getElementById("inset_cal_text");
        inset_cal_text.textContent = year + "/" + month;
        calendarHtml += '<table class="cal_table">';

        // 曜日の行を作成
        for (let i = 0; i < this.weeks_.length; i++) {
            calendarHtml += '<td class="cal_td">' + this.weeks_[i] + "</td>";
        }

        for (let w = 0; w < 6; w++) {
            calendarHtml += "<tr>";

            for (let d = 0; d < 7; d++) {
                if (w == 0 && d < startDay) {
                    // 1行目で1日の曜日の前
                    let num = lastMonthendDayCount - startDay + d + 1;
                    let preMonth = month;
                    if (month != 1) {
                        preMonth--;
                    } else {
                        preMonth = 12;
                    }
                    calendarHtml +=
                        `<td class="is-disabled cal_td selct_cal_data" id="cal_${year}_${preMonth}_${num}">` +
                        num
                    calendarHtml += `</td>`;

                } else if (dayCount > endDayCount) {
                    // 末尾の日数を超えた
                    let num = dayCount - endDayCount;
                    let nextMonth = month;
                    if (month != 12) {
                        nextMonth++;
                    } else {
                        month = 1;
                    }
                    calendarHtml +=
                        `<td class="is-disabled cal_td selct_cal_data" id="cal_${year}_${nextMonth}_${num}">` +
                        num;
                    calendarHtml += `</td>`;

                    dayCount++;
                } else {
                    if (year > 2020 && year <= 2030) {
                        calendarHtml +=
                            `<td class="cal_td this_month selct_cal_data" id="cal_${year}_${month}_${dayCount}">` +
                            dayCount;
                        calendarHtml += `</td>`;
                    }
                    dayCount++;
                }
            }
            calendarHtml += "</tr>";
        }
        calendarHtml += "</table>";

        return calendarHtml;
    }
    showCalendar() {
        const calendarHtml = this.createCalendar(this.year_, this.month_);
        const sec = document.createElement("section");
        sec.innerHTML = calendarHtml;
        document.querySelector("#calendar").appendChild(sec);
    }
    calculateYM(target) {
        if (target === "prev") {
            this.month_--;

            if (this.month_ < 1) {
                this.year_--;
                this.month_ = 12;
            }
        }

        if (target === "next") {
            this.month_++;

            if (this.month_ > 12) {
                this.year_++;
                this.month_ = 1;
            }
        }
    }
    selectCalDate(e) {
        // カレンダーから選択
        const splited_date = e.target.id.split("_");
        const a = [splited_date[1], splited_date[2], splited_date[3]];
        const date = a.join("/");

        // メモの部分
        // 選択した日付に色を塗る

        document.getElementById("insert_select_date").textContent = date;
        const targets = document.querySelectorAll(".selct_cal_data")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i].classList.contains("cal_check")) {
                targets[i].classList.remove("cal_check")
                break
            }
        }
        document.getElementById(e.target.id).classList.add("cal_check")
        document.getElementById("insert_cal_memo").textContent = date + "のメモ"
        const target = document.getElementById("cal_text_form")
        target.value = ""
    }


}