import { con } from "../../common/common";

class Cookie {
    getCookie(target) {
        // スタート時間を返す
        var cookies = document.cookie; //全てのcookieを取り出して
        var cookiesArray = cookies.split(";"); // ;で分割し配列に
        let start_time = "";
        let sub = " " + target;
        for (var c of cookiesArray) {
            //一つ一つ取り出して
            var cArray = c.split("="); //さらに=で分割して配列に
            let a = cArray[0].replace(" ", "")
            if (a == target) {
                // 取り出したいkeyと合致したら
                start_time = cArray[1]; // [key,value]
            }
        }
        return start_time;
    }
    calculateTime(s_time) {
        // クッキーからスタート時間を取って計算
        let start_time = s_time
        const end_time = new Date();
        start_time = parseInt(start_time);
        let num = Math.floor((end_time.getTime() - start_time) / 1000);
        let time = this.calculateOnleyTime(num);
        return {
            sconds: num,
            result: time,
        };
    }
    calculateOnleyTime(num) {
        // 時間を計算、文字列に直して返す
        let time_h = Math.floor((num % (24 * 60 * 60)) / (60 * 60));
        let time_m = Math.floor(((num % (24 * 60 * 60)) % (60 * 60)) / 60);
        let time_s = ((num % (24 * 60 * 60)) % (60 * 60)) % 60;
        let time = time_h + ":" + time_m + ":" + time_s;
        return time;
    }

}
export const cookie = new Cookie()
