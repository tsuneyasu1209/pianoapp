import {
    ev,
    con
} from "../../common/common"

export class Memo {
    constructor(from) {
        this.form_ = from
        this.count_ = 100 - from
        this.bloom_ = false
    }
    checkCalForm() {
        if (this.bloom_ == true) {
            // 後なん文字使えるか描画
            document.getElementById("cal_textar_span").textContent =
                "残り" + this.count_ + "文字";
        } else {
            this.count_ = this.count_ * -1;
            document.getElementById("cal_textar_span").textContent =
                this.count_ + "文字超え";
        }
    }
    countCalForm() {
        // 50文字いないならtrue
        if (0 <= this.count_) {
            this.bloom_ = true;
        } else {
            this.bloom_ = false;

        }
    }
}