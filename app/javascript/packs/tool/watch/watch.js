import {
    con,
    ev,
    cs
} from "../../common/common";
import {
    cookie
} from "../cookie/cookie"

let start_time

function insertTimerState(state) {
    // 状態を判断して文章を挿入


    switch (state) {
        case 0:
            insert_timer_state.textContent = "停止中"
            break;
        case 1:
            insert_timer_state.textContent = "稼働中"
            break;
        case 2:
            insert_timer_state.textContent = "一時停止"
            break;
        case 3:
            insert_timer_state.textContent = "保存出来ます"
            break;

        default:
            break;
    }
}

export class Watch {
    constructor() {

        this.timer_state_ = 0
        insertTimerState(this.timer_state_)

        cs.attachOrDetach("watch_start_btn", 1);

        ev.eventTargetId("watch_start_btn", "click", this.startWatch);
        ev.eventTargetId("watch_end_btn", "click", this.endWatch);
        ev.eventTargetId("watch_end_btn", "click", this.calculateLeaningTime);
    }

    startWatch() {
        // スタートボタンの処理
        const time = new Date();
        const result = time.getTime();
        const target = document.getElementById("insert_time");
        document.cookie = `startWatch=${result}: max-age=86400`; //一日で切れる
        start_time = result
        g_watch_timer = setInterval(() => {
            let a = cookie.calculateTime(start_time);
            target.textContent = a["result"];
        }, 1000);
        cs.attachOrDetach("watch_start_btn");
        setTimeout(function () {
            cs.attachOrDetach("watch_end_btn", 1);
            this["timer_state_"] = 1
            g_watch_state = 1
            insertTimerState(this["timer_state_"])
        }, 500);
    }

    endWatch() {
        // ストップボタンの処理
        cs.attachOrDetach("watch_end_btn");
        clearTimeout(g_watch_timer);
        setTimeout(function () {
            cs.attachOrDetach("watch_start_btn", 1);
            insertTimerState(this["timer_state_"])
            this["timer_state_"] = 3
            g_watch_state = 3
        }, 500);
    }
    changePouseOver() {
        // ポーズボタンに乗ってる時
        const target = document.querySelectorAll(".span_puse");
        for (let i = 0; i < target.length; i++) {
            target[i].style.backgroundColor = "#000";
        }
    }
    changePouseOut() {
        // ポーズボタンから離れる時
        const target = document.querySelectorAll(".span_puse");
        for (let i = 0; i < target.length; i++) {
            target[i].style.backgroundColor = "#fff";
        }
    }
    calculateLeaningTime() {
        // 計算してフォームに入れる
        let aaa = cookie.calculateTime(start_time)
        document.cookie = `startWatch=0; expires=Thu, 21 Mar 2019 12:00:00 GMT"`;
        document.cookie = `resultTime=0; expires=Thu, 21 Mar 2019 12:00:00 GMT"`;
        g_save_time = aaa["sconds"]; //保存する時間
    }

}