import {
    ev,
    con
} from "../../common/common";
import {
    Watch
} from "./watch.js";
import {
    Memo
} from "./memo";
import {
    Calendar
} from "../calendar/calendar";
import {
    cookie
} from "../cookie/cookie";
let watch = new Watch();
const calen = new Calendar();

function moveCalendar(e) {
    // 翌月前月の表示
    calen.calculateYM(e.target.id);
    if (calen["year_"] == 2020 && calen["month_"] == 12) {
        // 最小値
        calen["year_"]++;
        calen["month_"] = 1;
    } else if (calen["year_"] == 2030 && calen["month_"] == 1) {
        // 最大値
        calen["year_"]--;
        calen["month_"] = 12;
    } else {
        document.querySelector("#calendar").innerHTML = "";
        calen.showCalendar();
        ev.eventTargetAll(".selct_cal_data", "click", calen.selectCalDate);
    }
}

function memoFanc(e) {
    // メモに入力
    const memo = new Memo(e.target.value.length);
    memo.checkCalForm(memo.countCalForm());
}
ev.eventTargetId("cal_text_form", "keyup", memoFanc);
ev.eventTargetAll(".selct_cal_data", "click", calen.selectCalDate);
let timer_update = false;
$(function () {
    class DateInfo {
        constructor() {
            this.user_id_ = document.getElementById("user_id").textContent;
            let splited = document
                .getElementById("insert_select_date")
                .textContent.split("/");
            this.year_ = splited[0];
            this.month_ = splited[1];
            this.day_ = splited[2];
            const hiduke = new Date();
            this.today_year = hiduke.getFullYear();
            this.today_month = hiduke.getMonth() + 1;
            this.today_day = hiduke.getDate();
            this.today =
                this.today_year + "/" + this.today_month + "/" + this.today_day;
        }
    }

    function set_csrftoken() {
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            if (!options.crossDomain) {
                const token = $('meta[name="csrf-token"]').attr("content");
                if (token) {
                    return jqXHR.setRequestHeader("X-CSRF-Token", token);
                }
            }
        });
    }

    function insetTime(data, dom) {
        if (data == "") {
            document.getElementById(dom).textContent = "0:0:0";
        } else {
            document.getElementById(dom).textContent = data;
            let show = cookie.calculateOnleyTime(data);
            if (show != "NaN:NaN:NaN") {
                document.getElementById(dom).textContent = show;
            }
        }
    }

    function showError(jqXHR, textStatus, errorThrown) {
        console.log("ajax通信に失敗しました");
        console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
        console.log("textStatus     : " + textStatus); // タイムアウト、パースエラー
        console.log("errorThrown    : " + errorThrown.message); // 例外情報
        console.log("URL            : " + url);
    }

    let date_info = new DateInfo();

    function search_date(state = "0") {
        // データがあるか見つける
        date_info = new DateInfo();
        // 検索
        let year;
        let month;
        let day;
        if (state == "0") {
            // 月検索
            year = calen["year_"];
            month = calen["month_"];
        } else if (state == "1") {
            // 日にちで検索
            year = date_info["year_"];
            month = date_info["month_"];
            day = date_info["day_"];
        } else if (state == "2") {
            // 今日の検索
            year = date_info["today_year"];
            month = date_info["today_month"];
            day = date_info["today_day"];
        }
        $.ajax({
                type: "GET", // リクエストのタイプ
                url: "/search", // リクエストを送信するURL
                data: {
                    user_id: date_info["user_id_"],
                    year: year,
                    month: month,
                    day: day,
                    state: state,
                }, // サーバーに送信するデータ
                dataType: "json", // サーバーから返却される型
            })
            .done(function (data) {
                if (state == "0") {
                    // カレンダー表示
                    const target = document.querySelectorAll(".this_month");
                    for (let i = 0; i < data.length; i++) {
                        let text = "";
                        let col;
                        if (data[i]["memo"] != "") {
                            // メモに何か入っていれば印をつける
                            text += `<div class="check_memo" data-id="${data[i]["id"]}" id="memo_${data[i]["year"]}_${data[i]["month"]}_${data[i]["day"]}"></div>`;
                        } else {
                            text += `<div class="check_memo vanish" data-id="${data[i]["id"]}" id="memo_${data[i]["year"]}_${data[i]["month"]}_${data[i]["day"]}"></div>`;
                        }
                        if (data[i]["time"] != "0" || data[i]["time"] != "") {
                            col = calen.checkCal(data[i]["time"]);
                        }

                        target[data[i]["day"] - 1].insertAdjacentHTML("beforeend", text);
                        target[data[i]["day"] - 1].classList.add(col);
                    }
                } else if (state == "1") {
                    // 選択された日付のデータを出す
                    if (data.length != 0) {
                        document.getElementById("cal_text_form").value = data[0]["memo"];
                        // 選択した日の時間を挿入
                        insetTime(data[0]["time"], "insert_select_time");
                        g_update_flag = data.length;
                    } else {
                        g_update_flag = data.length;
                    }
                } else if (state == 2) {
                    // ページロードの時
                    if (data.length != 0) {
                        timer_update = true;
                        // アップデートなどに使う

                        if (data[0]["time"] == "") {
                            document.getElementById("js_form_time").textContent = "0:0:";
                        } else {
                            document.getElementById("js_form_time").textContent =
                                data[0]["time"];
                        }
                        insetTime(data[0]["time"], "insert_today_time")
                        document.getElementById("cal_text_form").value = data[0]["memo"];
                    } else {
                        timer_update = false;
                    }
                }
            })
            .fail(function () {
                showError(jqXHR, textStatus, errorThrown);
            });
    }

    function updateMemo() {
        set_csrftoken();
        let id = document.getElementById(
            `memo_${date_info["year_"]}_${date_info["month_"]}_${date_info["day_"]}`
        ).dataset.id;
        let memo = document.getElementById("cal_text_form").value;
        $.ajax({
                type: "PATCH", // リクエストのタイプ
                url: "/tools/" + id, // リクエストを送信するURL
                data: {
                    tool: {
                        memo: memo,
                    },
                }, // サーバーに送信するデータ
            })
            .done(function (data) {
                console.log(data); // dataを確認する
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                showError(jqXHR, textStatus, errorThrown);
            })
            .always(function () {
                $(".note_form-btn").prop("disabled", false); // submitボタンのdisableを解除
                $(".note_form-btn").removeAttr("data-disable-with"); // submitボタンのdisableを解除(Rails5.0以降はこちらも必要)
            });
    }

    function updateTimer() {
        set_csrftoken();
        let id = document.getElementById(
            `memo_${date_info["today_year"]}_${date_info["today_month"]}_${date_info["today_day"]}`
        ).dataset.id;
        let now_time = document.getElementById("js_form_time").textContent;
        g_save_time = parseInt(g_save_time) + parseInt(now_time);
        $.ajax({
                type: "PATCH", // リクエストのタイプ
                url: "/tools/" + id, // リクエストを送信するURL
                data: {
                    tool: {
                        time: g_save_time,
                    },
                }, // サーバーに送信するデータ
            })
            .done(function (data) {
                insetTime(data[0]["time"], "insert_today_time")
                g_save_time = 0
            })
            .fail(function (jqXHR, textStatus, errorThrown) {
                showError(jqXHR, textStatus, errorThrown);
            })
            .always(function () {
                $(".note_form-btn").prop("disabled", false); // submitボタンのdisableを解除
                $(".note_form-btn").removeAttr("data-disable-with"); // submitボタンのdisableを解除(Rails5.0以降はこちらも必要)
            });
    }

    function newMemo() {
        set_csrftoken();
        let memo = document.getElementById("cal_text_form").value;
        $.ajax({
                url: "/tools", // リクエストを送信するURLを指定
                type: "POST", // HTTPメソッドを指定（デフォルトはGET）
                data: {
                    // 送信するデータをハッシュ形式で指定
                    tool: {
                        year: date_info["year_"],
                        month: date_info["month_"],
                        day: date_info["day_"],
                        memo: memo,
                    },
                },
                dataType: "json", // レスポンスデータをjson形式と指定する
            })
            .done(function (data) {
                const targets = document.querySelectorAll(".check_memo");
                for (let i = 0; i < targets.length; i++) {
                    targets[i].remove();
                    g_update_flag = 1;
                }
                search_date();
            })
            .fail(function () {
                showError(jqXHR, textStatus, errorThrown);
            })
            .always(function () {
                $(".note_form-btn").prop("disabled", false); // submitボタンのdisableを解除
                $(".note_form-btn").removeAttr("data-disable-with"); // submitボタンのdisableを解除(Rails5.0以降はこちらも必要)
            });
    }

    function newTimer() {
        set_csrftoken();
        $.ajax({
                url: "/tools", // リクエストを送信するURLを指定
                type: "POST", // HTTPメソッドを指定（デフォルトはGET）
                data: {
                    // 送信するデータをハッシュ形式で指定
                    tool: {
                        year: date_info["year_"],
                        month: date_info["month_"],
                        day: date_info["day_"],
                        time: g_save_time,
                    },
                },
                dataType: "json", // レスポンスデータをjson形式と指定する
            })
            .done(function (data) {
                const targets = document.querySelectorAll(".check_memo");
                for (let i = 0; i < targets.length; i++) {
                    targets[i].remove();
                    g_update_flag = 1;
                }
                if (g_save_time == "") {
                    document.getElementById("js_form_time").textContent = "0";
                } else {
                    document.getElementById("js_form_time").textContent = g_save_time;
                }
                g_save_time = 0
                search_date();
            })
            .fail(function () {
                showError(jqXHR, textStatus, errorThrown);
            })
            .always(function () {
                $(".note_form-btn").prop("disabled", false); // submitボタンのdisableを解除
                $(".note_form-btn").removeAttr("data-disable-with"); // submitボタンのdisableを解除(Rails5.0以降はこちらも必要)
            });
    }
    $(".cal_btn").on("click", function (e) {
        // 翌月前月の表示
        moveCalendar(e);
        search_date();
        $(".selct_cal_data").on("click", function (e) {
            search_date("1");
        });
        // search_table_day()
    });

    $("#watch_memo_btn").on("click", function (e) {
        // 保存ボタンを押した時の処理
        if (g_watch_state == 1) {
            watch.endWatch()
            watch.calculateLeaningTime()
        }
        const val = document.getElementById("cal_text_form").value;
        const memo_send = new Memo(val.length);

        if (watch["timer_state_"] == 1) {
            if (memo_send["bloom_"] == true) {}
        }
        let now =
            date_info["year_"] + "/" + date_info["month_"] + "/" + date_info["day_"];
        e.preventDefault();
        if (timer_update == true && g_save_time != 0) {
            updateTimer();
        } else if (timer_update == false) {
            newTimer();
            timer_update = true;
        }

        // 現在の選択と今日の日付が一致した時
        if (g_update_flag == 1 || now == g_selected_date) {
            updateMemo();
        } else if (g_update_flag == 0) {
            newMemo();
            g_selected_date = now;
        }
    });
    $(".selct_cal_data").on("click", function () {
        // カレンダーの日付を選択した時
        search_date("1");
    });
    $(".cal_select_box").on("change", function () {
        // せ区とボックス
        calen["year_"] = document.getElementById("cal_date_select_year").value;
        calen["month_"] = document.getElementById("cal_date_select_month").value;
        document.querySelector("#calendar").innerHTML = "";
        calen.showCalendar();
        search_date();
    });
    search_date();
    search_date("2");
});