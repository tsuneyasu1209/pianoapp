import {
    cs,
    ev
} from "../../common/common"
import {
    sco
} from "../../common/socre";
import {
    keyPlay,
    loadAllKey
} from "../../play/play";
let all = loadAllKey()

class Opning {
    constructor() {
        // let sharps = document.querySelectorAll(`.sharp`);
        // for (let i = 0; i < sharps.length; i++) {
        //     sharps[i].classList.add("top_left")
        // }
        cs.attachOrDetach("oc_s")
        cs.attachOrDetach("score_board_block_5")
        cs.attachOrDetach(".single_block")
        cs.attachOrDetach(".button_block_center")
        cs.attachOrDetach(".insert_key", 1)
        cs.attachOrDetach(".button_block_box_unde", 1)
        cs.cahgeCss(".piano_wrap", "display: block");
    }
}

new Opning()

let score_count = 1

function select_key(e) {
    if (score_count >= 5) {
        sco.deleteScore()
        score_count = 1
    }

    let target = e.target.id;
    let symbol = sco.Judgment_of_sound(target);
    let va = sco.Judgment_of_8va(target);
    let key = [target];
    keyPlay(key);
    // 携帯用
    sco.drawScore(target, va, score_count, symbol);
    score_count++
}
ev.eventTargetAll(".key", "click", select_key)