import {
    con,
    ev,
    cs
} from "../../common/common";



;
(function (window) {

    var wa = {

        context: null,
        _buffers: {},


        _initialize: function () {
            this.context = new(window.AudioContext || window.webkitAudioContext)();
        },

        playSilent: function () {
            var context = this.context;
            var buf = context.createBuffer(1, 1, 22050);
            var src = context.createBufferSource();
            src.buffer = buf;
            src.connect(context.destination);
            src.start(0);
        },

        play: function (buffer) {
            // ファイル名で指定
            if (typeof buffer === "string") {
                buffer = this._buffers[buffer];
                if (!buffer) {
                    console.error('ファイルが用意できてません!');
                    return;
                }
            }

            var context = this.context;
            var source = context.createBufferSource();
            source.buffer = buffer;
            source.connect(context.destination);
            source.start(0);
        },

        loadFile: function (src, cb) {
            var self = this;
            var context = this.context;
            var xml = new XMLHttpRequest();
            xml.open('GET', src);
            xml.onreadystatechange = function () {
                if (xml.readyState === 4) {
                    if ([200, 201, 0].indexOf(xml.status) !== -1) {

                        var data = xml.response;

                        // webaudio 用に変換
                        context.decodeAudioData(data, function (buffer) {
                            // buffer登録
                            var s = src.split('/');
                            var b = s[s.length - 1];
                            let splited = b.split("-")
                            let key = splited[0]
                            self._buffers[key] = buffer;
                            // コールバック
                            cb(buffer);

                        });

                    } else if (xml.status === 404) {
                        // not found
                        console.error("not found");
                    } else {
                        // サーバーエラー
                        console.error("server error");
                    }
                }
            };

            xml.responseType = 'arraybuffer';

            xml.send(null);
        },

    };

    wa._initialize(); // audioContextを新規作成

    window.wa = wa;

}(window));
window.onload = function () {
    // ページ読み込みと同時にロード

    const target = document.getElementById("metro_sound").getAttribute('src')
    wa.loadFile(`${target}`, function (buffer) {});


}
const metronome_target_list = ["metronom_btn", "beat_target", ".metro_select_block"]
const bpm = document.getElementById("metronome_range")

function insertBpm(num) {
    const target = document.getElementById("show_bpm")
    target.textContent = "BPM" + num
}
export class Metronome {
    // メトロノームのクラス
    constructor() {
        ev.eventTargetId("metronome_range", "change", this.insertBpm)
        ev.eventTargetAll(".action_btn", "click", this.clickBtn)
    }
    insertBpm() {
        // バースライド
        insertBpm(bpm.value)
    }
    clickPlus() {
        // プラスボタン
        if (bpm != 220) {
            let a = parseInt(bpm.value) + 1
            bpm.value = a
            insertBpm(a)
        }
    }
    clickMinus() {
        // マイナスボタン
        if (bpm != 0) {
            let a = parseInt(bpm.value) - 1
            bpm.value = a
            insertBpm(a)
        }
    }
    clickBtn(e) {
        bpm.value = e.target.dataset.speed
        insertBpm(e.target.dataset.speed)
        let target = e.target
        let btns = document.querySelectorAll(".action_btn")
        for (let i = 0; i < btns.length; i++) {
            if (btns[i] == target) {
                btns[i].classList.add("top_select_code")
            } else {
                btns[i].classList.remove("top_select_code")

            }

        }
    }
    startMetronome() {
        // メトロノームを動かす
        const tempo = parseInt(bpm.value)
        cs.multipleAtOrDt(metronome_target_list)
        cs.attachOrDetach("metronom_stop_btn", 1)


        let max_beat = insertCircle()
        const dom_list = document.querySelectorAll(".beat_circle")
        g_beat = 0
        g_timer = setInterval(function () {
            countCircle(g_beat, dom_list)
            if (max_beat != g_beat) {
                g_beat++
            } else {
                g_beat = 0
            }
            wa.play("bpm")
        }, 60 / tempo * 1000);
    }
    stopMetronome() {
        // メトロノームを停止
        cs.attachOrDetach("metronom_stop_btn")
        cs.multipleAtOrDt(metronome_target_list, 1)

        // サークルを消す
        const all_circle = document.querySelectorAll('.beat_circle')
        for (let i = 0; i < all_circle.length; i++) {
            all_circle[i].remove()
        }
        clearTimeout(g_timer)
    }

}

function insertCircle() {
    const insert_taget = document.querySelector(".beat_count_block")
    const select_target = document.getElementById("beat_target").value
    const splited_num = select_target.split("_")

    // サークルを挿入
    let sentence = ""
    for (let i = 1; i <= splited_num[1]; i++) {
        sentence += `<div class="beat_circle" id="beat_circle_${i}"></div>`
    }
    insert_taget.insertAdjacentHTML("afterbegin", sentence);
    return splited_num[1] - 1
}

function countCircle(count, dom) {
    for (let i = 0; i < dom.length; i++) {
        if (count == i) {
            dom[i].classList.add("beat_circle_color")
        } else {
            dom[i].classList.remove("beat_circle_color")
        }
    }
}