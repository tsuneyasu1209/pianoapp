import {
    ev
} from "../../common/common"

import {
    Metronome
} from "./metronome.js"

const metro = new Metronome()

// メトロノームの処理
ev.eventTargetId("metronom_btn", "click", metro.startMetronome)
ev.eventTargetId("metronom_stop_btn", "click", metro.stopMetronome)
ev.eventTargetId("metro_plus", "click", metro.clickPlus)
ev.eventTargetId("metro_minus", "click", metro.clickMinus)

