import {
    ev,
    con
} from "../common/common"


class TopSelect {
    constructor() {
        ev.eventTargetAll(".top_select_title", "click", this.drowSelect)
        ev.eventTargetAll(".top_select_title", "click", this.selctTips)

    }
    drowSelect(e) {
        // 選択したものを一つだけ黄色く塗る
        let targets = document.querySelectorAll(".top_select_title")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i] == e.target) {
                targets[i].firstElementChild.classList.remove("vanish")
            } else {
                targets[i].firstElementChild.classList.add("vanish")
            }
        }
    }
    selctTips(e) {
        // 目次で移動

        const tips = document.querySelectorAll(".top_theme")
        let index = parseInt(e.target.dataset.select);
        let rect = tips[index].getBoundingClientRect();
        let position = window.pageYOffset + rect.top;
        scrollTo(0, position);
    }
}
new TopSelect()

let top_hight = 0
//196
let header_hight = 196
if (window.innerWidth < 1200) {
    header_hight = 30
}

function fixNav() {
    const sidebar = document.querySelector(".ad_section");

    if (top_hight == 0) {
        try {
            top_hight = document.querySelector(".top").clientHeight
        } catch (error) {}
        try {
            top_hight = document.querySelector(".theory_show_container").clientHeight
        } catch (error) {}

    }


    let side = document.querySelector(".top_select_block").clientHeight
    let side_psition = window.scrollY + side

    if (side_psition <= top_hight) {

        if (window.scrollY >= header_hight) {
            sidebar.style.paddingTop = window.scrollY - header_hight + "px";
        } else {
            sidebar.style.paddingTop = 0 + "px";
        }
    }

}

window.addEventListener("scroll", fixNav);