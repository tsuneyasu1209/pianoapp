import {
  cs,
  ev,
  con,
} from "../common/common";
import {
  com
} from "../common/func"

class Header {
  constructor() {

    ev.eventTargetAll(".header_non", "click", this.clickLink);
    ev.eventTargetAll(".header_non", "mouseover", this.mouseOver);
    ev.eventTargetAll(".header_non", "mouseout", this.mouseOut);
    ev.eventTargetAll(".header_under_content", "click", this.handleHeaderr);
    // //別の所押したら消える
    // document.addEventListener('click', (e) => {
    //   if(!e.target.closest('.header_under_content')) {
    //     const targets = document.querySelectorAll(".js_header_content")
    //     for (let i = 0; i < targets.length; i++) {
    //       targets[i].classList.add("vanish")
    //     }
    //   } 
    // })
    ev.eventTargetId("btn08", "click", this.clickHamburger)
    this.getScreenSize()
  }
  handleHeaderr(e) {

    const targets = document.querySelectorAll(".js_header_content")
    let num = parseInt(e.target.dataset.hed)
    for (let i = 0; i < targets.length; i++) {
      if (i == num) {
        if (targets[i].classList.contains("vanish")) {
          targets[i].classList.remove("vanish")
        } else {
          targets[i].classList.add("vanish")
        }
      } else {
        targets[i].classList.add("vanish")
      }
    }
  }

  clickLink(e) {
    try {
      let num = parseInt(e.target.dataset.link)
      let targets = document.querySelectorAll(".header_link")
      targets[num].click()
    } catch (error) {
      con("error")
    }

  }

  mouseOver(e) {
    let num = parseInt(e.target.dataset.link)
    if (0 == num) {
      document.getElementById("js_home_link").style.color = "black";
    } else if (1 == num) {
      document.getElementById("js_contact_link").style.color = "black";
    }
  }
  mouseOut(e) {
    let num = parseInt(e.target.dataset.link)
    if (0 == num) {
      document.getElementById("js_home_link").style.color = "white";
    } else if (1 == num) {
      document.getElementById("js_contact_link").style.color = "white";
    }
  }
  getScreenSize() {
    con("iii")
    if (window.innerWidth < 1200) {

    }
  }
  clickHamburger() {
    document.getElementById("btn08").classList.toggle("active")
    document.getElementById("js_hamburger_wrap").classList.toggle("vanish")

  }

}


new Header()