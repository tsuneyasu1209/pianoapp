import {
  cs,
  ev,
  con,
  getSoundRate,
  unDrowYellow
} from "../common/common";
import {
  keyEv
} from "../common/key";
import {
  show
} from "../common/rate";
import {
  com
} from "../common/func"
const error_code_target = document.querySelector(".profile_error_code");
export let error_code = error_code_target.textContent;
class MainOpning {
  constructor() {
    this.opningAtVanish();
  }
  opningAtVanish() {
    let uncheck = document.getElementById("ckey_num_0");
    if (uncheck.checked == true) {
      uncheck.checked = false;
    }
    uncheck = document.getElementById("key_num_0");
    if (uncheck.checked == true) {
      uncheck.checked = false;
    }
    let all_select_h_t = document.querySelectorAll(".select_h_t");
    let score_question_btn_block = document.querySelectorAll(
      ".score_question_btn_block"
    );
    for (let i = 0; i < all_select_h_t.length; i++) {
      all_select_h_t[i].classList.add("vanish");
    }
    for (let i = 0; i < score_question_btn_block.length; i++) {
      score_question_btn_block[i].classList.add("vanish");
    }
  }
}
new MainOpning();

function clearRateKey() {
  // 鍵盤に描画された記号を消す
  const keys = document.querySelectorAll(".key");
  for (let i = 0; i < keys.length; i++) {
    keys[i].textContent = "";
  }
}

function changeTheme(e) {
  // 音感、楽譜、コードで切り替え
  unDrowYellow(".key")
  now_display = e.target.value;
  let loop_list = [
    "profile_sound_block",
    "profile_score_block",
    "profile_code_block",
  ];
  cs.switchDisplay(loop_list, e.target.value);
  if (now_display == 0) {
    cs.attachOrDetach("show_sound_rate", 1);
    cs.attachOrDetach("anser_code_key");
    cs.cahgeCss(".piano_wrap", "display: block");
    // 音感
    if (error_code == 0) {
      result_list = getSoundRate("profile_sound_rate"); // グローバル変数
      result_list_only_key = show.getRateOnlyName(result_list);
      show.sortRate(result_list);
      // 問題の選択
    } else {
      cs.attachOrDetach("show_sound_rate", 0);
    }
  } else if (now_display == 1) {
    // 楽譜
    cs.cahgeCss(".piano_wrap", "display: block");
    cs.attachOrDetach("show_sound_rate", 1);
    cs.attachOrDetach("anser_code_key");
    clearRateKey();
  } else {
    // コードshow_sound_rate
    cs.attachOrDetach("show_sound_rate");
    cs.cahgeCss(".piano_wrap", "display: none");
    cs.attachOrDetach("anser_code_key", 1);
  }
  com.changeHight(".profile_wrap", ".mypage")
}
keyEv.ratePageKeyBtns();
ev.eventTargetAll(".select_theme", "change", changeTheme);