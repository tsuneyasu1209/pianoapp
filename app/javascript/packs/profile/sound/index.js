import { getSoundRate, con, ev, cs } from "../../common/common";
import { com } from "../../common/func";
import { show } from "../../common/rate";
import { error_code } from "../index.js";

let key_flag = 0; //鍵盤の有効化
let sound_op_num = 0; //問題の番号
let sound_question_state = 1; //1.追加　2.消去
const sound_question_title = document.getElementById("sound_question_title");
let chose_sound_range_op_state = 1; // 1:鍵盤指定, 2:範囲 3:キー 4:正解率

class SoundOpning {
  constructor() {
    this.black_key_list = this.getBlackKeyNum();
    ev.eventTargetId("key_select", "change", this.makeKey);
  }
  makeKey() {
    // キーを振り分け
    let base = [key_select.value];

    com.createKeyList(base);
    for (let i = 0; i < base.length; i++) {
      changeProcessingViaFlag(base[i], sound_question_state);
    }
  }
  getBlackKeyNum() {
    // 黒鍵の番号を取得
    let base_list = [5, 7, 10, 12, 14];
    let w_or_bresult_list = [5, 7, 10, 12, 14];
    for (let i = 1; i < 7; i++) {
      for (let k = 0; k < base_list.length; k++) {
        w_or_bresult_list.push(base_list[k] + 12 * i);
      }
    }
    return w_or_bresult_list;
  }
}
let sound_op = new SoundOpning();
class SoundKey {
  resetKey() {
    // 鍵盤を一旦全て元に戻す
    let color;
    let profile_keys = document.querySelectorAll(".key");
    for (let i = 0; i < profile_keys.length; i++) {
      color = com.juduge_w_or_b(profile_keys[i].id);
      cs.cahgeCss(profile_keys[i].id, `background-color: ${color}`);
    }
  }
  repaintKey(params) {
    // 選んだ問題を鍵盤に塗る
    for (let i = 0; i < params.length; i++) {
      params[i] = com.changeSPToShrarp(params[i]);
      // 選んだ問題に色を塗る
      cs.cahgeCss(params[i], "background-color: yellow");
    }
  }
}
let sound_key = new SoundKey();

class SoundSelect {
  constructor() {}

  drowKeys(result_list) {
    // 鍵盤に確率ごとに色を塗る
    try {
      sound_key.repaintKey(chose_key_list[sound_op_num][1]);
    } catch (error) {}
    // 正解率からの振り分け
    show.sortRate(result_list);
  }
  showNowQestion() {
    // 現在の問題を表示
    let use_list = com.sliceQuestions("sound_qestion_edit");

    // 問題が登録されている時の処理
    com.insertSelectBoxContent(use_list, "sound_question_select_box");
    return use_list;
  }
  showChosedSoundQuestion() {
    // 選択した問題を表示
    let use_chose_key_list;
    use_chose_key_list = chose_key_list[sound_op_num][1].slice(
      0,
      chose_key_list[sound_op_num][1].length
    );
    if (use_chose_key_list == "") {
      use_chose_key_list = ["問題が選択されていません"];
    }
  }
}
let sound_sel = new SoundSelect();

class SoundAdd {
  constructor() {
    ev.eventTargetAll(".add_or_delete", "click", this.chageFlag);
  }
  addQuestion(target) {
    // 問題の選択
    let target_key = target;
    let sarch_result = chose_key_list[sound_op_num][1].indexOf(target_key);
    let empty_index = chose_key_list[sound_op_num][1].indexOf("");

    if (sarch_result == -1) {
      chose_key_list[sound_op_num][1].push(target_key);
      this.insertSoundQestionForm();
      if (empty_index != -1) {
        chose_key_list[sound_op_num][1].splice(empty_index, 1);
      }
      // 選んだ問題に色を塗る
      cs.cahgeCss(target_key, "background-color: yellow");
      // 更新ボタンを押すよう注意を促す
      cs.attachOrDetach("changed_attention", 1);
      sound_sel.showChosedSoundQuestion();
    }
  }
  // 追加と消去のフラグを切り替える
  chageFlag(e) {
    let target_key = e.target;
    if (target_key.classList.contains("add_question")) {
      sound_question_state = 1;
      cs.attachOrDetach("all_clear_btn_of_sound_question", 0);
    } else if (target_key.classList.contains("delete_question")) {
      sound_question_state = 2;
      cs.attachOrDetach("all_clear_btn_of_sound_question", 1);
    }
  }
  deleteQuestionKey(target) {
    let color;
    const target_key = target;
    if (target_key.match(/p/)) {
      color = "black";
    } else {
      color = "#FAF5E1";
    }
    cs.cahgeCss(`${target_key}`, `${color}`);
    this.deleteTargetSoundQuestion(target);
    // 更新ボタンを押すよう注意を促す
    cs.attachOrDetach("changed_attention", 1);
  }
  deleteTargetSoundQuestion(target) {
    // 選択した問題を消す
    if (sound_question_state == 2) {
      let target_key = target;

      // try {
      let sarch_key = target_key;
      sarch_key = com.changeSPToShrarp(sarch_key);
      let result = chose_key_list[sound_op_num][1].indexOf(sarch_key);
      chose_key_list[sound_op_num][1].splice(result, 1); // ２番目から１つ削除
      this.deleteInsertSoundQuestionForm();
      // } catch (TypeError) {
      //   console.log("noting");
      // }
    }
  }
  // 選択した問題をフォームに挿入
  insertSoundQestionForm() {
    const sound_qestion_edit = document.getElementById("sound_qestion_edit");
    let join_list = [];
    for (let i = 0; i < chose_key_list.length; i++) {
      let join_text = `${chose_key_list[i][0]}:${chose_key_list[i][1].join(
        "/"
      )}`;
      join_list.push(join_text);
    }
    let question = join_list.join("-");
    sound_qestion_edit.value = question;
  }
  deleteInsertSoundQuestionForm() {
    // 消した問題をフォームに挿入
    for (let i = 0; i < chose_key_list[sound_op_num][1].length; i++) {
      chose_key_list[sound_op_num][1][i] = com.changeSPToShrarp(
        chose_key_list[sound_op_num][1][i]
      );
    }
    this.insertSoundQestionForm(chose_key_list[sound_op_num][1]);
  }
}
let sound_add = new SoundAdd();

function getCheckedNum(e) {
  key_flag = 1;
  sound_key.resetKey();
  // 問題タイトル選択が変更されたら
  sound_op_num = e.target.value;
  cs.attachOrDetach("profile_sound_rate_block", 1);
  cs.attachOrDetach("common_btn_block", 1);

  showQestionTitle();
  sound_sel.showChosedSoundQuestion();
  try {
    sound_key.repaintKey(chose_key_list[sound_op_num][1]);
  } catch (TypeErrorror) {
    sound_key.resetKey();
  }
}

function changeQuestion(e) {
  // 追加と消去で分ける
  if (key_flag == 1) {
    if (sound_question_state == 1) {
      sound_add.addQuestion(e.target.id);
    } else if (sound_question_state == 2) {
      sound_add.deleteQuestionKey(e.target.id);
    }
  }
}

// 問題のタイトルをインプットタグに挿入
function showQestionTitle() {
  sound_question_title.value = chose_key_list[sound_op_num][0];
}

// タイトルの変更があればその変える
function changeQuestionTitle() {
  chose_key_list[sound_op_num][0] = sound_question_title.value;
  sound_add.insertSoundQestionForm();
}

function resetAllQestions() {
  // 問題を全て消す
  chose_key_list[sound_op_num][1] = [];
  sound_add.insertSoundQestionForm();
  sound_sel.showChosedSoundQuestion();
  sound_key.resetKey();
}

function changeSoundRangeNum(e) {
  // 範囲指定の状態 chose_sound_range_op_state 1:鍵盤指定, 2:範囲 3:キー 4:正解率
  let loop_list = [
    "sound_one_of_key",
    "sound_range_block",
    "questions_select_range_block",
    "sound_rate_range_select_block",
  ];
  let target_key = e.target;
  let list_num = target_key.value - 1;
  chose_sound_range_op_state = target_key.value;
  cs.switchDisplay(loop_list, list_num);
  if (sound_range_list.length == 0) {
    cs.cahgeCss("sound_range_text_st", "display: block");
  } else {
    cs.cahgeCss("sound_range_text_end", "display: block");
  }
}
let w_or_b = 1;
function chageWOrBState(e) {
  // 範囲選択の時に白鍵黒鍵両方から選択
  let target_key = e.target;
  if (target_key.classList.contains("chose_w_b_1")) {
    w_or_b = 1;
  } else if (target_key.classList.contains("chose_w_b_2")) {
    w_or_b = 2;
  } else {
    w_or_b = 3;
  }
}

function whiteOrblack(params, black_key_list) {
  // 黒鍵白鍵のジャッジ
  let result_index = black_key_list.indexOf(params);
  if (result_index != -1) {
    return 2;
  } else {
    return 1;
  }
}

function changeProcessingViaFlag(i, state) {
  // 追加か消去で処理を変える 1:追加 2:消去
  let target_key = document.querySelector(`.k_${i}`);
  if (target_key == null) {
    target_key = document.getElementById(`${i}`);
  }
  if (state === 1) {
    sound_add.addQuestion(target_key.id);
    // 選んだ問題に色を塗る
    cs.cahgeCss(target_key.id, "background-color: yellow");
  } else {
    sound_add.deleteQuestionKey(target_key.id);
  }
}

function rangeDrow(range_st, range_end) {
  // 範囲選択の始まりと終わりで色を塗る
  let b_result = 0;
  for (let i = range_st; i <= range_end; i++) {
    b_result = whiteOrblack(i, sound_op["black_key_list"]);

    if (w_or_b === b_result) {
      changeProcessingViaFlag(i, sound_question_state);
    } else if (w_or_b === 3) {
      changeProcessingViaFlag(i, sound_question_state);
    }
  }
}
let sound_range_list = [];
function choseRangeKey(e) {
  // 範囲選択の処理
  if (chose_sound_range_op_state == 2) {
    let loop_list = ["sound_range_text_st", "sound_range_text_end"];
    if (sound_range_list.length == 0) {
      let start_key = document.getElementById(`${e.target.id}`);
      let start_key_num = start_key.className.split("k_");
      sound_range_list.push(parseInt(start_key_num[1]));
      cs.switchDisplay(loop_list, 1);
    } else if (sound_range_list.length == 1) {
      // 黒鍵の数値をゲット
      let end_key = document.getElementById(`${e.target.id}`);
      let end_key_num = end_key.className.split("k_");
      sound_range_list.push(parseInt(end_key_num[1]));
      cs.switchDisplay(loop_list, 0);
      // 昇順降順
      if (sound_range_list[0] <= sound_range_list[1]) {
        rangeDrow(sound_range_list[0], sound_range_list[1]);
      } else if (sound_range_list[0] >= sound_range_list[1]) {
        rangeDrow(sound_range_list[1], sound_range_list[0]);
      }
      con(sound_range_list)
      sound_range_list = [];
    }
  }
}
function addSoundRateRange(params_list) {
  // 正解率で選んだキーを追加
  let rate_list = [];
  const sound_rate_range_input_st = document.getElementById(
    "sound_rate_range_input_st"
  );
  const sound_rate_range_input_end = document.getElementById(
    "sound_rate_range_input_end"
  );
  let start_num = sound_rate_range_input_st.value;
  let end_num = sound_rate_range_input_end.value;
  if (start_num !== "" && end_num !== "") {
    let splied_list = com.splitList(params_list);
    for (let i = 2; i < splied_list.length; i = i + 3) {
      if (
        parseInt(start_num) <= parseInt(splied_list[i]) &&
        parseInt(end_num) >= parseInt(splied_list[i])
      ) {
        changeProcessingViaFlag(splied_list[i - 2], sound_question_state);
      }
    }
  }
}
function clickShowRate(e) {
  // 選んだ鍵盤に正解率が記録されているか
  let insert_dom_list = [
    ".rate_table_selected_key",
    ".rate_table_qestion_times",
    ".rate_table_rate",
  ];
  let target = e.target.id;
  let key_index = result_list_only_key.indexOf(target); //glo

  if (key_index != -1) {
    // 入っている場合
    let splied_list = result_list[key_index].split(","); //glo
    splied_list[0] = com.changeShartToP(splied_list[0]);
    splied_list[1] += "回";
    splied_list[2] += "%";
    for (let i = 0; i < insert_dom_list.length; i++) {
      const target = document.querySelector(`${insert_dom_list[i]}`);
      target.textContent = splied_list[i];
    }
  } else {
    let none_list = [target, "0回", "0%"];
    none_list[0] = com.changeShartToP(none_list[0]);
    // let error_code = error_code_target.textContentgeShartToP(none_list[0]);
    for (let i = 0; i < insert_dom_list.length; i++) {
      const target = document.querySelector(`${insert_dom_list[i]}`);
      target.textContent = none_list[i];
    }
  }
}

cs.cahgeCss(".piano_wrap", "display: block");
ev.eventTargetAll(".key", "click", choseRangeKey);
ev.eventTargetAll(".key", "click", changeQuestion);
// ev.eventTargetAll(".key", "click", clickShowRate);

// 開幕確率を取得
// 確率が入っているかどうかの判断
if (error_code == 0) {
  result_list = getSoundRate("profile_sound_rate"); // グローバル変数
  sound_sel.drowKeys(result_list);
  result_list_only_key = show.getRateOnlyName(result_list); //グローバル変数

  // 問題の選択
} else {
  cs.attachOrDetach("show_sound_rate", 0);
}
let chose_key_list = sound_sel.showNowQestion();
// 一番上のタイトルを挿入
showQestionTitle();
// 問題タイトル選択されたら
// タイトルの変更があればその変える
ev.eventTargetId("sound_question_title", "change", changeQuestionTitle);
// 問題タイトル選択されたら
ev.eventTargetId("sound_question_select_box", "change", getCheckedNum);

ev.eventTargetAll(".chose_sound_range_op", "click", changeSoundRangeNum);
// 範囲選択の時に白鍵黒鍵両方から選択
ev.eventTargetAll(".chose_w_b", "click", chageWOrBState);
// 正解率で選んだキーを追加
ev.eventTargetNoNameAll(
  ".sound_rate_range_input",
  "change",
  addSoundRateRange,
  result_list
);

// 問題を消す処理
ev.eventTargetAll(".add_or_delete", "click", sound_add.chageFlag);
ev.eventTargetAll(".all_clear_btn", "click", resetAllQestions);
