import { con, cs } from "../../common/common";
import { com } from "../../common/func";
export class CodeOpning {
  constructor() {
    this.key_name = [
      "C",
      "C♯",
      "D",
      "D♯",
      "E",
      "F",
      "F♯",
      "G",
      "G♯",
      "A",
      "A♯",
      "B",
    ];
    this.plus_num_list = [
      [4, 3], // M
      [4, 3, 4], //M7
      [3, 4], //m
      [3, 4, 3], //m7
      [3, 3, 4], //m7-5
      [5, 2], //sus4
      [3, 3, 3], //dim7
      [4, 4], //aug
      [4, 3, 2], //6
      [4, 3, 7], //add9
      [4, 3, 3], //7
    ];
    this.drow_key_num = this.calCodeNum(this.plus_num_list, 0);
    com.drowCodeKey(this.drow_key_num);
    this.result_storage_ob = this.makeVoidStorate();
    this.ob = this.getFormValue(this.result_storage_ob);
    this.non = com.getScoreRateNon(".code_rate");
    if (this.non == 11) {
      let add_list = [
        ".question_key_selct_code",
        ".code_rate_list",
        ".code_rate_under",
      ];
      cs.multipleAtOrDt(add_list)
    }
  }
  calCodeNum(key_num_list, base_num) {
    // codeに使う鍵盤番号を計算
    let result_num_list = [];
    let last_result_list = [];
    for (let i = 0; i < key_num_list.length; i++) {
      let cal_num = base_num;
      result_num_list = [base_num];
      for (let k = 0; k < key_num_list[i].length; k++) {
        cal_num += key_num_list[i][k];
        result_num_list.push(cal_num);
      }
      last_result_list.push(result_num_list);
    }
    return last_result_list;
  }

  makeVoidStorate() {
    let result_storage_ob = {};
    for (let i = 0; i < 11; i++) {
      result_storage_ob[`ckey_num_${i}`] = [];
    }
    return result_storage_ob;
  }
  getFormValue(ob) {
    let score_rate = document.querySelectorAll(".code_rate");
    for (let i = 0; i < score_rate.length; i++) {
      ob[`ckey_num_${i}`] = score_rate[i].value;
    }
    return ob;
  }
}
