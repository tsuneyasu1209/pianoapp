import { ev, cs, con, unDrowYellow } from "../../common/common";
import { CodeOpning } from "./opning";
import { show } from "../../common/rate";
import { ck } from "../../common/checbox";

let code_op = new CodeOpning();
let selected_key_num;
class Select {
  constructor() {
    ev.eventTargetAll(".code_result_list", "click", this.selectRate);
  }
  validateCheck(e) {
    let target = e.target;
    ck.changeChecked(".check_code", target);
    let a = target.id.split("_");
    selected_key_num = a[2];
  }

  selectRate(e) {
    // 確率を押すと鍵盤に描画される

    unDrowYellow(".key");
    let anser;
    try {
      const a = e.target.id.split("_");
      const key_num = a[1];
      let num = parseInt(key_num);
      let code = [code_op.plus_num_list[selected_key_num]];
      anser = code_op.calCodeNum(code, num);
      cs.attachOrDetach(".check_code_error");
      for (let i = 0; i < anser[0].length; i++) {
        cs.cahgeCss(`code_key_${anser[0][i]}`, "background-color: yellow");
      }
    } catch (error) {
      cs.attachOrDetach(".check_code_error", 1);
    }
  }
}

let code_sel = new Select();

function drowCodeRateKeys(e) {
  // キーごとに正解率を表示させる
  result_list = code_op["ob"][e.target.id].split("/");
  show.showCodeRate(code_op["ob"][e.target.id], code_op["key_name"]);
  result_list_only_key = show.getRateOnlyName(result_list);
  show.sortRate(result_list);
  code_sel.validateCheck(e);
}

ev.eventTargetAll(".check_code", "click", drowCodeRateKeys);
ev.eventTargetAll(".key", "click", show.clickShowRate);
