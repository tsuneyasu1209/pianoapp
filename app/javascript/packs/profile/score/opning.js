import { cs, con } from "../../common/common";
import { com } from "../../common/func";
export class ScoreOpning {
  constructor() {
    this.drowSharpAndFlat();
    this.rate_storage_ob = this.makeVoidStorate();
    this.ob = this.getFormValue(this.rate_storage_ob);
    this.non = com.getScoreRateNon(".score_rate");
    if (this.non == 15) {
      cs.attachOrDetach(".question_key_selct_score");
      cs.attachOrDetach("show_sound_rate");

    }
  }

  drowSharpAndFlat() {
    // ＃と♭を開幕描画
    const key_selct_mini_score = document.querySelectorAll(
      ".key_selct_mini_score"
    );
    function insert(params, sorb, num = 0) {
      for (let i = 1; i <= 7; i++) {
        let insert_span = "";
        for (let k = 1; k <= i; k++) {
          let sentence = `<span class="mini_sharpand_flat ${params}_${k}">${sorb}</span>`;
          insert_span += sentence;
        }
        key_selct_mini_score[i + num].insertAdjacentHTML(
          "afterbegin",
          insert_span
        );
      }
    }
    insert("mini_sharp mini_score_symbol_sharp", "#");
    insert("mini_flat mini_score_symbol_flat", "♭", 7);
  }
  makeVoidStorate() {
    let result_storage_ob = {};
    for (let i = 0; i < 15; i++) {
      result_storage_ob[`key_num_${i}`] = [];
    }
    return result_storage_ob;
  }
  getFormValue(ob) {
    let score_rate = document.querySelectorAll(".score_rate");
    for (let i = 0; i < score_rate.length; i++) {
      ob[`key_num_${i}`] = score_rate[i].value;
    }
    return ob;
  }
}
