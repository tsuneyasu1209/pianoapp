import { ev, con } from "../../common/common";
import { ScoreOpning } from "./opning";
import { Select } from "./select";
import { show } from "../../common/rate";


let score_op = new ScoreOpning();
let score_sel = new Select();
function drowscoreRateKeys(e) {
  // キーごとに正解率を表示させる
  result_list = score_op["ob"][e.target.id].split("/");
  result_list_only_key = show.getRateOnlyName(result_list);
  show.sortRate(result_list);
  score_sel.validateCheck(e)
}

ev.eventTargetAll(".check_key", "click", drowscoreRateKeys);
ev.eventTargetAll(".key", "click", show.clickShowRate);
