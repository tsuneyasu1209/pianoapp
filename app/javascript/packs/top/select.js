import {
    ev,
    con
} from "../common/common"
import {
    com
} from "../common/func"

function attachVanishAll() {
    let targets = document.querySelectorAll(".js_top_del")
    // com.changeHight('.top_content', '.top')
    // document.querySelector('.top').style.height = '100vh';
    for (let i = 0; i < targets.length; i++) {
        targets[i].classList.add("vanish")
    }
}
export class TopSelect {
    constructor() {
        ev.eventTargetAll(".top_select_content", "click", this.drowSelect)
        ev.eventTargetAll(".top_select_content", "click", this.showContents)
        ev.eventTargetAll(".score_basic_title", "click", this.showScoreBasic)
    }
    drowSelect(e) {
        // 選択したものを一つだけ黄色く塗る
        let targets = document.querySelectorAll(".top_select_content")
        for (let i = 0; i < targets.length; i++) {
            if (targets[i] == e.target) {
                targets[i].firstElementChild.classList.remove("vanish")
            } else {
                targets[i].firstElementChild.classList.add("vanish")
            }
        }
    }
    showContents(e) {
        // 選んだ中身を出す
        let targets = document.querySelectorAll(".content_section")
        let num = parseInt(e.target.dataset.select)
        attachVanishAll()
        for (let i = 0; i < targets.length; i++) {
            if (i == num) {
                targets[i].classList.remove("vanish")
                if (i == 0) {
                    document.querySelector(".basic_knowledge_select_block").classList.remove("vanish")
                }
            } else {
                targets[i].classList.add("vanish")

            }

        }
        // com.changeHight('.top_content', '.top')
    }
    showScoreBasic(e) {
        let targets = document.querySelectorAll(".score_basic_knowledge_content")
        let btns = document.querySelectorAll(".score_basic_title")
        let num = parseInt(e.target.dataset.scoreBasic)
        for (let i = 0; i < targets.length; i++) {
            if (i == num) {
                targets[i].classList.remove("vanish")
                btns[i].classList.add("top_select_code")
            } else {
                targets[i].classList.add("vanish")
                btns[i].classList.remove("top_select_code")

            }

        }
        // com.changeHight('.top_content', '.top')
    }
}