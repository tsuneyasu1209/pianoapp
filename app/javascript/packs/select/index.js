import {
    unDrowYellow
} from "../common/common"

function changeSelect() {
    let start = document.getElementById("start_target")
    let end = document.getElementById("end_target")
    let error = document.querySelector('.select_error')
    start_num = parseInt(start.value.substr(1, 1), 10)
    end_num = parseInt(end.value.substr(1, 1), 10)

    error.style.cssText = "display: none";

    function dorwColor(opacity = "opacity: 1", color = '#FAF5E1', de_start = 0, de_end = 9) {
        for (let i = de_start; i < de_end; i++) {
            let keys = document.querySelectorAll(`.mini_key_${i}`)
            for (let k = 0; k < keys.length; k++) {
                keys[k].style.cssText = opacity;
                keys[k].style.backgroundColor = color;
            }
        }
    }
    if (start_num <= end_num) {
        dorwColor("opacity: 1", '#FC001C', start_num, end_num + 1)
        unDrowYellow(".skey")

    } else {
        error.style.cssText = "display: block";
    }
}

function gameStart() {
    let select_range_content = select_range()
    if (select_range_content[0] == 1) {
        createArray(select_range_content[1], select_range_content[2])
        question_array = make_questions_selected_times()
        questionsSoundPlay(question_array)
    }
}


const btn = document.querySelector('.start_btn')
btn.addEventListener("click", gameStart);
// セレクト範囲の変更があった時の処理
const range_start = document.getElementById('start_target')
range_start.addEventListener("change", changeSelect)
const range_end = document.getElementById('end_target')
range_end.addEventListener("change", changeSelect)