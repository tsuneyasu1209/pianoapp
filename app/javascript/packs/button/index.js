import {
  questionsSoundPlay,
  make_questions_selected_times,
} from "../common/sound";
import {
  cal,
  show
} from "../common/rate";
import {
  sco
} from "../common/socre";
import {
  cs,
  ev
} from "../common/common";
// もう一度選ぶを押した時の処理
function oneMoreChose() {
  let keys = document.querySelectorAll(".piano_block");
  let select_block = document.querySelector(".select_block");
  let sore_content = document.getElementById("sore_content");
  let key_select_btns = document.querySelectorAll(".key_select_btn");
  let one_more_btns = document.querySelectorAll(".one_more");
  let next_question_btn = document.querySelector(".next_question_btn");
  let piano = document.querySelector(".piano_wrap")

  for (let i = 0; i < key_select_btns.length; i++) {
    key_select_btns[i].style.cssText = "display: none";
  }

  for (let i = 0; i < keys.length; i++) {
    if (keys[i].classList.contains("delete_target")) {
      keys[i].classList.remove("delete_target");
    }
  }
  cs.attachOrDetach(".select_block", 1)
  cs.attachOrDetach(".mini_piano", 1)

  select_block.style.cssText = "display: block";
  sore_content.style.cssText = "display: none";
  piano.style.cssText = "display: none";

  next_question_btn.style.cssText = "display: none";
  for (let i = 0; i < one_more_btns.length; i++) {
    one_more_btns[i].style.cssText = "display: none";
  }
  sco.deleteScore(question_array);
  anser_array = []
  num_array = []
  questions_num = 0
  questions_count = 1
  score_count = 1
  // 正解率を入れるリスト
  result_list = []
  // 正解率を計算するフラグ
  times_of_anser = 1
  question_array = []
  document.getElementById("insert_question_num").textContent = "第１問"

}

// 答えを表示
function showAnser(question_array) {
  anser_array = [];
  cs.cahgeCss(".show_anser_btn", "display: none");
  cs.cahgeCss(".next_question_btn", "display: block");
  for (let i = 0; i < question_array.length; i++) {
    let symbol = sco.Judgment_of_sound(question_array[i]);
    va = sco.Judgment_of_8va(question_array[i]);
    sco.drawScore(question_array[i], va, score_count, symbol);
    anser_array.push("o");
    score_count++;
  }

  questionsSoundPlay(question_array);

  for (let i = 0; i < question_array.length; i++) {
    let judge = "no";
    if (times_of_anser == 1) {
      cal.calculateCodeReusult(judge, result_list, question_array[i]);
    }
  }
  anser_array = [];
  score_count = 1;
  times_of_anser = 1;
}

// 次の問題へquestions_count
function anserToNext() {
  cs.cahgeCss(".show_anser_btn", "display: block");
  cs.cahgeCss(".next_question_btn", "display: none");

  sco.deleteScore(question_array);
  if (questions_num == questions_count) {
    show.showRate();
    // 結果出た後の聞けるようにする処理
    var rates = document.querySelectorAll(".sound_result");
    rates.forEach((rate) => {
      rate.addEventListener("click", show.selecteRate);
    });
    rates.forEach((rate) => {
      rate.addEventListener("mouseover", show.dorwKeyColor);
    });
    rates.forEach((rate) => {
      rate.addEventListener("mouseout", show.deleteKeyColor);
    });

    questions_count = 1;
  } else {
    question_array = make_questions_selected_times();
    questionsSoundPlay(question_array);
    questions_count++;
    cs.insertQuestionsNum("insert_question_num", questions_count);
  }
}

// もう一度選ぶボタン
ev.eventTargetId(".one_more_select_btn", "click", oneMoreChose);
// 次の問題へ
ev.eventTargetId(".next_question_btn", "click", anserToNext);
// 答えを表示

const show_anser_btn = document.querySelector(".show_anser_btn");
show_anser_btn.addEventListener(
  "click",
  function () {
    showAnser(question_array);
  },
  false
);

// もう一度問題を聞く
const one_more_listen = document.querySelector(".one_more_listen_btn");
one_more_listen.addEventListener(
  "click",
  function () {
    questionsSoundPlay(question_array);
  },
  false
);

let target_block_list = ["A0", "C1", "C2", "C3", "C4", "C5", "C6", "C7", "C8"];

function limitPianoBlock(e) {
  // keyを限定させる処理
  let count_num = 0;
  let target = "." + e.target.textContent + "_block";
  const limit_target_block = document.querySelector(`${target}`);
  limit_target_block.classList.toggle("vanish");
  for (let i = 0; i < target_block_list.length; i++) {
    let all_key_blocks = document.querySelector(
      `.${target_block_list[i]}_block`
    );
    let target_btn = document.getElementById(`${target_block_list[i]}_btn`);
    if (all_key_blocks.classList.contains("vanish")) {
      target_btn.classList.remove("all_on");
    } else {
      target_btn.classList.add("all_on");
      count_num++;
    }
  }
  // allボタンをの処理
  const all_btn = document.getElementById("all_btn");
  if (count_num == 9) {
    all_btn.classList.add("all_on");
  } else {
    all_btn.classList.remove("all_on");
  }
}

function limitAllBlock() {
  // ALLボタンを押した時の処理
  const target = document.getElementById("all_btn");
  target.classList.toggle("all_on");
  for (let i = 0; i < target_block_list.length; i++) {
    let target_btn = document.getElementById(`${target_block_list[i]}_btn`);
    if (target.classList.contains("all_on")) {
      target_btn.classList.add("all_on");
      let change_btn = document.querySelector(`.${target_block_list[i]}_block`);
      change_btn.classList.remove("vanish");
    } else {
      target_btn.classList.remove("all_on");
      let change_btn = document.querySelector(`.${target_block_list[i]}_block`);
      change_btn.classList.add("vanish");
    }
  }
}

function chengeSingleAll(e) {
  // 鍵盤を一つにするか全て使うか
  const select_octaves = document.querySelectorAll(".select_octave");
  const target = e.target.id;
  const add_or_remove_list_1 = [".button_block_box_unde", ".insert_key"];
  const add_or_remove_list_2 = [".single_block"];

  if (target == "oc_s") {
    cs.listAttachOrDetach(add_or_remove_list_1, add_or_remove_list_2);
  } else {
    cs.listAttachOrDetach(add_or_remove_list_2, add_or_remove_list_1);
  }

  for (let i = 0; i < select_octaves.length; i++) {
    if (select_octaves[i].id == target) {
      select_octaves[i].classList.add("all_on");
    } else {
      select_octaves[i].classList.remove("all_on");
    }
  }
}
ev.eventTargetAll(".limit_target_btn", "click", limitPianoBlock);
ev.eventTargetAll(".select_octave", "click", chengeSingleAll);

ev.eventTargetId("all_btn", "click", limitAllBlock);