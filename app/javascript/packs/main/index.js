import {
    cs,
    ev
} from "../common/common";

function topScroll() {
    if (document.scrollingElement.scrollTop < 10) {
        document.scrollingElement.scrollTop = 0;
    } else {
        document.scrollingElement.scrollTop = document.scrollingElement.scrollTop / 2;
        setTimeout(topScroll, 10);
    }
}
const nav = document.getElementById('js_header');


function fixNav() {
    if (window.scrollY >= 10) {
        document.getElementById("to_top").classList.remove("vanish")
    } else if (window.scrollY <= 10) {
        document.getElementById("to_top").classList.add("vanish")
    }
}

window.addEventListener('scroll', fixNav);
ev.eventTargetId("to_top", "click", topScroll)