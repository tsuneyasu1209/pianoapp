import {
  checkAndPlay,
  con,
  cs,
  ev
} from "../common/common";
import {
  com,
  Vol
} from "../common/func";
import {
  keyEv
} from "../common/key";
import {
  sco
} from "../common/socre";
import {
  cal,
  show
} from "../common/rate";
import {
  ck
} from "../common/checbox";
import {
  PointBar
} from "../common/point_bar";

let question_times; //問題数
let game_start_flag = 0; // 0: off 1:on
let base_t_h_list_index; //base_t_h_listリストのインデックス番号を取得
let all_key; // 選んだキーを入れるリスト
let question_and_anser_list; // 0:key 1:question 2:anser
let anser_array = []; //答えを入れるリスト
let times_of_anser = 1; // 正解率を計算するフラグ
let questions_count = 1;
let point = 0
let login = com.checUserLogin();
let point_bar = new PointBar(login, "point_score_form_edit")
new Vol();

export class Opning {
  constructor() {
    // // 開幕実行する関数
    // ２−５小節目を消去
    for (let i = 2; i < 6; i++) {
      cs.attachOrDetach(`score_board_block_${i}`, 0);
    }
    // 確率画面で不要なリスト
    this.vanish_list = [
      ".select_h_t",
      "score_question_btn_block",
      ".score_question_block",
    ];

    this.base = this.cKeyNum();
    // 0:両方 1:heon 2:ト音のリスト
    this.base_t_h_list = this.getToonAndHeonKeyNum(this.base);
    con(this.base_t_h_list)
    this.key_num_list = this.createKeyAnser();
    // キーごとに正解津を収納できる空オブジェクトを作成
    this.result_storage_ob = this.makeVoidStorate();
    this.target_key_list = [
      "score_rate_edit_c",
      "score_rate_edit_g",
      "score_rate_edit_d",
      "score_rate_edit_a",
      "score_rate_edit_e",
      "score_rate_edit_b",
      "score_rate_edit_fp",
      "score_rate_edit_cp",
      "score_rate_edit_f",
      "score_rate_edit_bn",
      "score_rate_edit_en",
      "score_rate_edit_an",
      "score_rate_edit_dn",
      "score_rate_edit_gn",
      "score_rate_edit_cn",
    ];
  }

  cKeyNum() {
    let base = [4];
    com.createKeyList(base);
    base.splice(0, 0, 1, 3);
    return base;
  }

  makeVoidStorate() {
    let result_storage_ob = {};
    for (let i = 0; i < 15; i++) {
      result_storage_ob[`key_num_${i}`] = [];
    }
    return result_storage_ob;
  }
  dorwSoreKey(params) {
    const a = document.querySelectorAll(".score_on_sharp_block_1");
    const b = document.querySelectorAll(".score_on_flat_block_1");
    // ♯と♭の描画
    let splited_item = params.split("_");
    if (1 <= splited_item[2] && splited_item[2] < 8) {
      for (let i = 0; i < splited_item[2]; i++) {
        a[i].classList.remove("vanish");
      }
    } else if (splited_item[2] > 7) {
      for (let i = 0; i < splited_item[2] - 7; i++) {
        b[i].classList.remove("vanish");
      }
    }
  }

  getToonAndHeonKeyNum(base) {
    // へ音と音のリストを返す
    let heon_list = [];
    let toon_list = [];
    let both_list = [];
    for (let i = 0; i < base.length; i++) {
      if (base[i] < 32) {
        heon_list.push(base[i]);
      } else if (base[i] > 47) {
        toon_list.push(base[i]);
      } else {
        both_list.push(base[i]);
      }
    }
    heon_list = heon_list.concat(both_list);
    toon_list = both_list.concat(toon_list);
    return [base, heon_list, toon_list];
  }
  createKeyAnser() {
    // 半音上がる鍵盤をオブジェクトに格納
    let key_num_list = {};
    let sharp_key = ["f", "c", "g", "d", "a", "e", "b"];
    let flat_key = ["b", "e", "a", "d", "g", "c", "f"];

    function createKey(key_list, params = 0, params2 = 8, param3 = 0) {
      for (let i = params; i < params2; i++) {
        key_num_list[`key_num_${i}`] = [];
        for (let k = 0; k < i - param3; k++) {
          key_num_list[`key_num_${i}`].push(key_list[k]);
        }
      }
    }
    createKey(sharp_key);
    createKey(flat_key, 8, 15, 7);
    return key_num_list;
  }
}
let op = new Opning();

// スタートボタンに使う関数
// let check_key_flag = 0; // 0:選択画面 1:正解率の画面

class Select {
  constructor() {
    ev.eventTargetAll(".validate_check", "click", this.varidateCheckedKey);
    // チェックボックsを全て選択、又ははずす
    ev.eventTargetAll(
      ".all_check_or_uncheck",
      "click",
      this.scoreQuestionAllBtn
    );
  }
  varidateCheckedKey(e) {
    // 会員以外は一つしか選択出来ないようにする
    const target = e.target;

    function switchKeyCheckBox(target) {
      if (target.classList.contains("check_key")) {
        ck.changeChecked(".check_key", target);
      } else {
        ck.changeChecked(".check_toon_heon", target);
      }
    }
    ck.checkUnckeck(".validate_check");
    if (login == 0 || check_key_flag == 1) {
      // ログインしてない、又は正解率の画面の時
      switchKeyCheckBox(target);
    }
  }

  scoreQuestionAllBtn(e) {
    // チェックボックsを全て選択、又ははずす
    const target = e.target;
    let check_list = [".check_key", ".check_toon_heon"];
    ck.allCheckOrUncheck(check_list, target);
  }
  getQuestionTimes() {
    // 問題を何回繰り返すかの取得
    const question_score_num = document.getElementsByName("question_score_num");
    for (let i = 0; i < question_score_num.length; i++) {
      if (question_score_num[i].checked) {
        question_times = question_score_num[i].value;
      }
    }
  }
}

let sel = new Select();

class ChoseValue {
  constructor() {
    this.all_key = this.getCheckedValu(".check_key"); //選んだキーをリストに入れる
    this.base_t_h_list_index = this.getBaseListIndex();
  }
  getCheckedValu(target) {
    // チェックされいる要素のidをリストで返す
    const all_check_key = document.querySelectorAll(target);
    let checked_value_list = [];
    for (let i = 0; i < all_check_key.length; i++) {
      if (all_check_key[i].checked) {
        checked_value_list.push(all_check_key[i].id);
      }
    }
    return checked_value_list;
  }
  getBaseListIndex() {
    // 両方、へ音、と音に別れたリストのどれを使うかを返す
    let chose_t_ore_h = this.getCheckedValu(".s_t_or_h"); //へ音ト音の選択
    let num = 0;
    if (chose_t_ore_h.length == 2) {
      num = 0;
    } else if (chose_t_ore_h.length == 1) {
      if (chose_t_ore_h[0] == "ckeck_h") {
        num = 1;
      } else {
        num = 2;
      }
    }
    return num;
  }
}
let cho = new ChoseValue();

// 問題を作るクラス
class MakeQuestion {
  createScoreQuestion(chose_key_list, key_list, base, mi = 13, ma = 68) {
    // 問題と正解のリストを作る
    const min = mi;
    const max = ma;
    let question_key_nums_list = []; //ランダムなキー
    let question_nums_list = []; //楽譜に描画するよう
    let anser_list = []; //　キーにあった答え

    for (let i = 0; i < 1; i++) {
      let num = Math.floor(Math.random() * base.length);
      // num = 15;

      const random_key_num = Math.floor(Math.random() * chose_key_list.length);
      let question_num = base[num];

      // ベースとなる鍵盤の頭文字取得
      let key = document.querySelector(`.k_${base[num]}`).id.substr(0, 1);
      let splited_key_num = chose_key_list[random_key_num].split("_");
      // 半音ずれるか
      let anser_num = question_num;
      let index_num = key_list[chose_key_list[random_key_num]].indexOf(key);
      if (index_num != -1) {
        if (1 <= splited_key_num[2] && splited_key_num[2] < 8) {
          anser_num++;
        } else if (splited_key_num[2] > 7) {
          anser_num--;
        }
      }
      question_nums_list.push(document.querySelector(`.k_${base[num]}`).id);
      question_key_nums_list.push(chose_key_list[random_key_num]);
      anser_list.push(document.querySelector(`.k_${anser_num}`).id);
    }

    let question_and_anser_list = [
      question_key_nums_list,
      question_nums_list,
      anser_list,
    ];
    return question_and_anser_list;
  }
  drowQuestion(params) {
    // 楽譜に描画
    for (let i = 0; i < params.length; i++) {
      let symbol;
      // 両方、へ音、ト音の選択でシンボルをかえる
      if (base_t_h_list_index == 0) {
        symbol = sco.Judgment_of_sound(params[i]);
      } else if (base_t_h_list_index == 1) {
        symbol = "heon_";
      } else {
        symbol = "toon_";
      }
      let va = sco.Judgment_of_8va(params[i]);
      sco.drawScore(params[i], va, i + 1, symbol);
    }
  }
  playQestions(all_key) {
    // 問題を作る一連の処理
    let error_f = 0; //errorが出たら再度回すために使う
    let n = 0;
    while (n < 10) {
      n++;
      try {
        question_and_anser_list = que.createScoreQuestion(
          all_key,
          op["key_num_list"],
          op["base_t_h_list"][base_t_h_list_index]
        );
        error_f = 0;
      } catch (error) {
        error_f = 1;
      }
      if (error_f == 0) {
        break;
      }
    }
    console.log(question_and_anser_list[2]);
    this.drowQuestion(question_and_anser_list[1]);
    op.dorwSoreKey(question_and_anser_list[0][0]);
  }
}
let que = new MakeQuestion();

class Anser {
  // 答えを選ぶ
  constructor(ele) {
    // シングルオクターブの時の挙動
    ele = keyEv.singeKeyAnser(octave, ele, question_and_anser_list[2][0]);
    anser_array.push(ele);
    checkAndPlay(ele);
  }
}

class CheckAnser {
  constructor() {
    let judge;
    if (question_and_anser_list[2] == anser_array[0]) {
      judge = "ok";
    } else {
      judge = "no";
    }
    if (times_of_anser == 1) {
      cal.calculateCodeReusult(
        judge,
        op["result_storage_ob"][question_and_anser_list[0][0]],
        question_and_anser_list[2][0]
      );
      point = point_bar.calculatePoint(judge, point)
      point_bar.levleBar(judge, point_bar["point_list"]) //レベルバーの更新

    }
    anser_array = [];
    if (judge == "ok") {
      // 正解の場合
      sco.deleteScore();
      que.playQestions(all_key);
      point_bar.insertProgressBar(questions_count) //進行度のバーを更新
      questions_count++;
      times_of_anser = 1;
      cs.insertQuestionsNum("insert_score_qusetion_num", questions_count);
    } else {
      // 不正解の時
      times_of_anser++;
    }
  }
}
class AfterAnser {
  constructor() {
    if (
      question_times < questions_count
      // 1 < questions_count
    ) {
      // 問題が全て終わった時確率を計算する
      const add_list = [".score_question_block"];
      const remove_list = [".select_key_block", ".score_rate_block"];
      cs.listAttachOrDetach(add_list, remove_list);
      keyEv.ratePageKeyBtns();
      ck.allClreCheckBox(".check_key");
      cs.multipleAtOrDt(op["vanish_list"]);
      let result_rate_list = cal.getInsertRate(op["result_storage_ob"]);
      let vanish_target_key_list = show.getTargetCheckBox(
        op["result_storage_ob"]
      );
      if (login == 1) {
        cal.soreAndCodeFormInsert(op["target_key_list"], result_rate_list);
        point_bar.insertPointForm(point)
      }
      game_start_flag = 0; //gameをoff鍵盤を無効化
      check_key_flag = 1; //正解率のパートを有効に
    }
  }
}

function scoreGameStart() {
  let h_t_check_count = ck.checkError(".s_t_or_h");
  let check_key_count = ck.checkError(".check_key");
  if (h_t_check_count >= 1 && check_key_count >= 1) {
    // スタートボタン押した時の処理
    base_t_h_list_index = cho.getBaseListIndex();
    all_key = cho.getCheckedValu(".check_key"); //選んだキーをリストに入れる
    con(all_key)
    sel.getQuestionTimes();
    point_bar["progress_with"] = point_bar.getProgressWidth(question_times) //進行バーをどれぐらいすすめるか

    que.playQestions(all_key);
    game_start_flag = 1; //フラグを立てて鍵盤を有効に

    // error文を消去
    cs.cahgeCss(".piano_wrap", "display: block");
    cs.attachOrDetach(".score_question_block", 1);
    const add_list = [
      ".select_key_block",
      `.s_t_or_h_error`,
      `.check_key_error`,
    ];
    cs.multipleAtOrDt(add_list);
  }
}
// 答に使う関数
function selectAnsers(e) {
  if (game_start_flag == 1) {
    let target = e.target.id;
    con(target)
    new Anser(target);
    new CheckAnser();
    new AfterAnser();
  }
}

// 確率に使う関数
function showSocreKeyRate(e) {
  if (check_key_flag == 1) {
    // キーごとに正解率を表示させる
    result_list = op["result_storage_ob"][e.target.id].split("/");
    result_list_only_key = show.getRateOnlyName(result_list);
    show.sortRate(result_list);
  }
}

// ＃と♭を開幕描画
// 会員以外は一つしか選択出来ないようにする
// 問題終了後キーを押すと結果がresult_listに入る
ev.eventTargetAll(".validate_check", "click", showSocreKeyRate);
// スタートボタン処理
ev.eventTargetId("score_start_btn", "click", scoreGameStart);
// オクターブで使う鍵盤の数を決める
ev.eventTargetAll(".select_octave", "click", keyEv.swithOctave);

// 答えを選択
ev.eventTargetAll(".key", "click", selectAnsers);
ev.eventTargetAll(".key", "click", show.clickShowRate);