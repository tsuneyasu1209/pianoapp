import {
  cs,
  con,

} from "../common/common";

import {
  GetNotice
} from "../common/notice";

const top_theme = document.querySelectorAll(".top_theme")
let theme_hight_list = []

function moveCheck(params) {
  if (theme_hight_list.length == 0) {
    for (let i = 0; i < top_theme.length; i++) {
      theme_hight_list.push(window.pageYOffset + top_theme[i].getBoundingClientRect().top)

    }
  }
  let index = 0

  if (0 <= window.scrollY && window.scrollY <= theme_hight_list[1]) {
    index = 0
  } else if (theme_hight_list[1] <= window.scrollY && window.scrollY < theme_hight_list[2]) {
    index = 1
  } else if (theme_hight_list[2] <= window.scrollY && window.scrollY < theme_hight_list[3]) {
    index = 2
  } else if (theme_hight_list[3] <= window.scrollY && window.scrollY < theme_hight_list[4]) {
    index = 3
  }
  let targets = document.querySelectorAll(".top_select_title")

  for (let i = 0; i < targets.length; i++) {
    if (i == index) {
      targets[i].firstElementChild.classList.remove("vanish")
    } else {
      targets[i].firstElementChild.classList.add("vanish")
    }
  }
}
window.addEventListener("scroll", moveCheck);

let load_text = new GetNotice();
window.onload = function () {
  if (load_text["load_flag"] != 1) {
    cs.attachOrDetach(".loading_block")
    cs.attachOrDetach("load_block", 1)

    setTimeout(() => {
      document.getElementById("load_block").classList.add("out_flame")
      cs.attachOrDetach(".top", 1)
    }, 8000);

  } else {
    cs.attachOrDetach(".loading_block")
    cs.attachOrDetach(".top", 1)
  }
}