import { con } from "../common/common";

$(function () {

    // メモ投稿(POSTメソッド)の処理
    function set_csrftoken() {
        $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
            if (!options.crossDomain) {
                const token = $('meta[name="csrf-token"]').attr('content');
                if (token) {
                    return jqXHR.setRequestHeader('X-CSRF-Token', token);
                }
            }
        });
    }
    $("#tool_from").on("click", function (e) {
        e.preventDefault(); // デフォルトのイベント(HTMLデータ送信など)を無効にする
        let inputyear = $("#watch_year").val(); // textareaの入力値を取得
        let inputmonth = $("#watch_month").val(); // textareaの入力値を取得
        let inputday = $("#watch_day").val(); // textareaの入力値を取得
        let inputtime = $("#watch_time").val(); // textareaの入力値を取得
        let inputText = $("#watch_memo").val(); // textareaの入力値を取得
        set_csrftoken();
        $.ajax({
                url: '/tools', // リクエストを送信するURLを指定
                type: "POST", // HTTPメソッドを指定（デフォルトはGET）
                data: { // 送信するデータをハッシュ形式で指定
                    tool: {
                        year: inputyear,
                        month: inputmonth,
                        day: inputday,
                        memo: inputText,
                        time: inputtime
                    }
                },
                dataType: "json" // レスポンスデータをjson形式と指定する
            })
            .done(function (data) {
                console.log(data)
            })
            .fail(function () {
                alert("error!"); // 通信に失敗した場合はアラートを表示
            })
            .always(function () {
                $(".note_form-btn").prop("disabled", false); // submitボタンのdisableを解除
                $(".note_form-btn").removeAttr("data-disable-with"); // submitボタンのdisableを解除(Rails5.0以降はこちらも必要)
            });
    });
    $('#update').on('click', function () {
        set_csrftoken();
        let id = 26
        let year = "2021"
        let month = "2"
        let day = "23"
        let time = "150"
        let memo = "悲しい"
        $.ajax({
                type: "PATCH", // リクエストのタイプ
                url: "/tools/" + id, // リクエストを送信するURL
                data: {
                    tool: {
                        year: year,
                        month: month,
                        day: day,
                        time: time,
                        memo: memo,

                    }
                }, // サーバーに送信するデータ
            })
            .done(function (data) {
                console.log(data); // dataを確認する
            }).fail(function (jqXHR, textStatus, errorThrown) {
                // 通信失敗時の処理

                console.log("ajax通信に失敗しました");
                console.log("jqXHR          : " + jqXHR.status); // HTTPステータスが取得
                console.log("textStatus     : " + textStatus); // タイムアウト、パースエラー
                console.log("errorThrown    : " + errorThrown.message); // 例外情報
                console.log("URL            : " + url);
            });
    })
    $('#search').on('click', function () {
        // 検索
        let user_id = 22
        let year = 2020
        let month = 11
        $.ajax({
                type: 'GET', // リクエストのタイプ
                url: '/search', // リクエストを送信するURL
                data: {
                    user_id: user_id,
                    year: year,
                    month: month
                }, // サーバーに送信するデータ
                dataType: 'json' // サーバーから返却される型
            })
            .done(function (data) {
                return data
            }).fail(function () {
                alert("error!"); // 通信に失敗した場合はアラートを表示
            })
    })
    // メモ削除(DELETEメソッド)の処理
//  メモ削除(DELETEメソッド)の処理
 $(".notes").on("click", ".note_delete", function(e) {
    set_csrftoken();

    e.preventDefault();  // デフォルトのイベント（リンクURLへの遷移処理など）を無効にする
    e.stopPropagation();  // 現在のイベントのさらなる伝播（DELETEメソッドの実行）を止める
    let url = $(this).attr("href");
    $.ajax({
      url: url,
      type: "POST",  // 原則に従って"DELETE"メソッドを使用しない
      data: {
        _method: "delete",  // ここで"DELETE"メソッドを使用することを指定
      },
      dataType: "json"
    })
    .done(function(data) {
      $("#note" + data.id).remove();  // レスポンスデータのIDを元に投稿を削除
    })
    .fail(function(XMLHttpRequest) {
      alert(XMLHttpRequest.status);
    });
  });

});
