docker-compose run web rails new . --force --no-deps --database=postgresql
docker-compose build

railsのデータベース設定
# database.yml
default: &default
  adapter: postgresql
  encoding: unicode
  host: db
  username: postgres
  password: password
  pool: 5

development:
  <<: *default
  database: myapp_development


test:
  <<: *default
  database: myapp_test

docker-compose run web rake db:create
docker-compose up
localhost:3000

vueを使う時は
docker-compose run web rails webpacker:install:vue
yarn add vuex
yarn add vue-router
yarn add axios
bin/webpack-dev-server

railsコマンド
docker-compose run web bundle install
docker-compose run web rails #

docker-compose exec # bash

<%= javascript_pack_tag 'hello_vue' %>
<%= stylesheet_pack_tag 'hello_vue' %>

rails g model Memo year:string month:string day:string content:string